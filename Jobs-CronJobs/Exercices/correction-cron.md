
## Correction

### 1. Création d'un Pod Mongo

La spécification suivante définit le Pod *db* basé sur *mongo:4.0*.

```
apiVersion: v1             
kind: Pod                  
metadata:
  name: db
  labels:
    app: db
spec:
  containers:
  - name: mongo
    image: mongo:4.0
```

Copiez cette spécification dans *mongo-pod.yaml* et lancez ce Pod avec la commande:

```
kubectl apply -f mongo-pod.yaml
```

Note: vous pouvez également utiliser la commande impérative suivante pour créer le Pod *db*

```
kubectl run db --image=mongo:4.0
```

### 2. Exposition de la base Mongo

La spécification suivante définit le Service *db* de type *ClusterIP*. Ce service permet d'exposer le Pod précédent à l'intérieur du cluster.

```
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: db
  type: ClusterIP
  ports:
  - port: 27017
```

Copiez cette spécification dans *mongo-svc.yaml* et lancez ce Service avec la commande:

```
kubectl apply -f mongo-svc.yaml
```

Note: vous pouvez également créer ce Service avec la commande impérative suivante:

```
kubectl expose pod/db --port 27017 --target-port 27017
```
### 3. Ajout d'un label sur l'un des nodes du cluster

```
kubectl get nodes
```
puis
```
kubectl label node minikube app=dump
```

### 4. Définition d'un Job pour effectuer le dump de la base de données

La spécification suivante définit un Job qui effectue le dump de la base de données.

```
apiVersion: batch/v1
kind: Job
metadata:
  name: dump
spec:
  template:
    spec:
      restartPolicy: Never
      nodeSelector:
        app: dump
      containers:
      - name: mongo
        image: mongo:4.0
        command:
        - /bin/bash
        - -c
        - mongodump --gzip --host db --archive=/dump/db.gz
        volumeMounts:
        - name: dump
          mountPath: /dump
      volumes:
      - name: dump
        hostPath:
          path: /dump
```

Copiez cette spécification dans *mongo-dump-job.yaml* et lancez ce Job avec la commande:

```
kubectl apply -f mongo-dump-job.yaml
```

Après quelques secondes on peut vérifier que le Pod lancé par le Job est dans l'état *Completed*:

```
kubectl get po
```
```
NAME         READY   STATUS      RESTARTS   AGE
dump-qjh5x   0/1     Completed   0          32s
```

Nous pouvons également regarder les logs du Pods afin d'avoir la confirmation que le dump a été effectué correctement:

```
kubectl logs dump-r5jg6
```
```
2024-07-05T01:52:42.877+0000    writing admin.system.version to archive '/dump/db.gz'
2024-07-05T01:52:42.880+0000    done dumping admin.system.version (1 document)

```

### 5. Définition d'un CronJob pour effectuer le dump de la base de données à intervalle régulier

La spécification suivante définit un CronJob qui effectue le dump de la base de données, accessible via le service nommé *db*, toutes les minutes.

```
apiVersion: batch/v1
kind: CronJob
metadata:
  name: dump
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          nodeSelector:
            app: dump
          containers:
          - name: mongo
            image: mongo:4.0
            command:
            - /bin/bash
            - -c
            - mongodump --gzip --host db --archive=/dump/$(date +"%Y%m%dT%H%M%S")-db.gz
            volumeMounts:
            - name: dump
              mountPath: /dump
          restartPolicy: OnFailure
          volumes:
          - name: dump
            hostPath:
              path: /dump
```

Copiez cette spécification dans *mongo-dump-cronjob.yaml* et lancez ce CronJob avec la commande:

```
kubectl apply -f mongo-dump-cronjob.yaml
```

### 6. Vérification des dump

La commande suivante lance le Pod *test* demandé:

```
apiVersion: v1
kind: Pod
metadata:
  name: test
spec:
  nodeSelector:
    app: dump
  containers:
  - name: test
    image: alpine:3.15
    command:
    - "sleep"
    - "10000"
    volumeMounts:
    - name: dump
      mountPath: /dump
  volumes:
  - name: dump
    hostPath:
      path: /dump
```

Lancez un shell interactif dans le container du pod *test*:

```
kubectl exec -ti test -- sh
```

Depuis ce shell, vous pourrez observer les dumps créés

```
# ls /dump
20240705T015401-db.gz  20240705T015402-db.gz  20240705T015501-db.gz  20240705T015502-db.gz  db.gz
```

### 7. Vérification des dumps (autre méthode)

La commande suivante permet de lancer un Pod de debug dont l'unique container *alpine* sera lancé dans les namespaces pid et network du node NODE_NAME. Le système de fichiers du node sera automatiquement monté dans le répertoire */host* du container:

```
kubectl debug node/minikube -it --image=alpine
```

Les dumps sont donc présents dans */host/dump* depuis le container *alpine*.

### 8. Cleanup

La commande suivante permet de supprimer les différentes ressources créées:

```
kubectl delete job/dump cj/dump po/test po/db svc/db
```
