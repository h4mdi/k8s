## Exercice

Dans cet exercice, vous allez créer un Pod et l'exposer à l'extérieur du cluster en utilisant un Service de type *LoadBalancer*.

Note: cet exercice ne peut être réalisé que si votre cluster est provisionné chez un cloud provider (Exoscale, DigitalOcean, GKE, EKS, ...).

### 1. Création d'un Pod

Créez un fichier *ghost.yaml* définissant un Pod ayant les propriétés suivantes:
- nom: *ghost*
- label associé au Pod: *app: ghost*
- nom du container: *ghost*
- image du container: *ghost:4*

Créez ensuite le Pod spécifié dans *ghost.yaml*.

### 2. Définition d'un service de type LoadBalancer

Créez un fichier *ghost-lb.yaml* définissant un service ayant les caractéristiques suivantes:
- nom: *ghost-lb*
- type: *LoadBalancer*
- un selector permettant le groupement des Pods ayant le label *app: ghost*.
- exposition du port *80*
- forward des requêtes vers le port *2368* des Pods sous-jacents (port d'écoute de l'application *ghost*)

Créez ensuite le Service spécifié dans *ghost-lb.yaml*

### 3. Adresse IP associée au service

Assurez-vous qu'une adresse IP externe est associée au Service *ghost-lb* (cela peut prendre quelques dizaines de secondes).

### 4. Accès au Service

Lancez un navigateur sur l'adresse IP externe du Service (il s'agit de l'adresse IP associé au LoadBalancer créé pour exposer le service à l'extérieur). Cela vous permettra d'accéder au Pod dans lequel tourne l'application *ghost*


### 5. Cleanup

Supprimez l'ensemble des ressources créés dans cet exercice.

