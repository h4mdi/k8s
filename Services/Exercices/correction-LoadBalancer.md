
## Correction

### 1. Création du Pod

La spécification du Pod est la suivante:

```
apiVersion: v1
kind: Pod
metadata:
  name: ghost
  labels:
    app: ghost
spec:
  containers:
  - name: ghost
    image: ghost:4
```

La commande suivante permet de créer le Pod:

```
kubectl apply -f ghost.yaml
```

### 2. Définition d'un Service de type LoadBalancer

La spécification du Service demandé est la suivante:

```
apiVersion: v1
kind: Service
metadata:
  name: ghost-lb
  labels:
    app: ghost
spec:
  selector:
    app: ghost
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 2368
```

La commande suivante permet de lancer le Service:

```
kubectl apply -f ghost-lb.yaml
```

Exécutez le tunnel dans un terminal séparé.

```bash
minikube tunnel
```

La commande `minikube tunnel` s'exécute en tant que processus, créant une route réseau sur l'hôte vers le cluster en utilisant l'adresse IP du cluster comme passerelle.

La commande `tunnel` expose l'IP externe directement à tout programme s'exécutant sur le système d'exploitation hôte.

### 3. Adresse IP associée au service

La commande suivante permet d'obtenir quelques informations relatives au service *ghost-lb* dont l'adresse IP externe associée:

```
kubectl get svc ghost-lb
```


### 5. Cleanup

Les ressources peuvent être supprimées avec les commandes suivantes:

```
kubectl delete po/ghost
kubectl delete svc/ghost-lb
```

