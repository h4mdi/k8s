## Créer un Service à partir d'un Fichier YAML
Appliquer un fichier de configuration YAML pour créer un service:
```sh
kubectl apply -f <fichier-service.yaml>
```

Exemple de fichier YAML pour un service ClusterIP:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: www
spec:
  selector:
    app: www
  type: ClusterIP
  ports:
    - port: 80
      targetPort: 80
```

Exemple de fichier YAML pour un service NodePort:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-nodeport
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      type: NodePort
```

Dans cet exemple, un service de type NodePort nommé "nginx-nodeport" est défini. Il est associé au sélecteur d'étiquette `app: nginx`, ce qui signifie qu'il s'appliquera aux pods portant cette étiquette. Le service expose le port 80 en utilisant le protocole TCP, avec un mappage vers le port 80 sur les pods ciblés. La spécification `type: NodePort` indique que ce service sera accessible via un port NodePort, dont le numéro sera automatiquement attribué dans la plage par défaut, généralement entre 30000 et 32767.

Cependant, il est également possible de spécifier un port NodePort personnalisé, comme illustré ci-dessous :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-nodeport
spec:
  selector:
    app: nginx
  ports:
    - port: 80
      targetPort: 80
      nodePort: 31000
  type: NodePort
```
Dans cette configuration, le port NodePort est fixé à 31000, permettant un accès spécifique à ce port pour le service "nginx-nodeport". Il est important de noter que la sélection manuelle du port NodePort peut parfois être nécessaire pour des raisons spécifiques à l'environnement ou aux politiques de sécurité.


Exemple de fichier YAML pour un service LoadBalancer:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-loadbalancer-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer

```

## Lister les Services
Lister tous les services dans le cluster:
```sh
kubectl get services
```

Lister les services dans un namespace spécifique:
```sh
kubectl get services -n <nom-du-namespace>
```

## Décrire un Service
Obtenir des détails sur un service:
```sh
kubectl describe service <nom-du-service>
```

## Supprimer un Service
Supprimer un service:
```sh
kubectl delete service <nom-du-service>
```

## Trouver le ClusterIP d'un Service
Obtenir l'adresse IP interne d'un service:
```sh
kubectl get service <nom-du-service> -o jsonpath='{.spec.clusterIP}'
```

## Résumé des Services
Lister tous les objets de type service dans le cluster:
```sh
kubectl get svc
```

---

## Obtenir l'URL pour accéder au service (spécifique à Minikube)

La commande `minikube service nginx-nodeport --url` s'exécute en tant que processus, créant un tunnel vers le cluster. Cette commande expose le service directement à tout programme s'exécutant sur le système d'exploitation hôte.

```bash
    minikube service nginx-nodeport --url
```

Exécutez le tunnel dans un terminal séparé.

```bash
minikube tunnel
```

La commande `minikube tunnel` s'exécute en tant que processus, créant une route réseau sur l'hôte vers le CIDR de service du cluster en utilisant l'adresse IP du cluster comme passerelle.

La commande `tunnel` expose l'IP externe directement à tout programme s'exécutant sur le système d'exploitation hôte.


