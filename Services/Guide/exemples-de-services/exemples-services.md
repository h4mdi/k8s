Dans cett partie, nous allons explorer en détail la mise en réseau dans Kubernetes en utilisant NGINX comme exemple. Vous apprendrez à utiliser la ressource Service pour exposer des Pods, tant à l'intérieur du cluster qu'à l'extérieur, ainsi que les différents types de services disponibles pour répondre à des besoins spécifiques de connectivité.

### 1- Présenter la ressource Service

Un Service Kubernetes est une abstraction qui définit un ensemble de Pods et une politique d'accès à ces Pods. Il existe plusieurs types de Services :
- **ClusterIP** : Accès interne au cluster uniquement.
- **NodePort** : Exposition du service via un port fixe sur chaque nœud.
- **LoadBalancer** : Exposition via un load balancer externe (généralement sur un cloud provider).

#### Pratique
1. **Créer un Service ClusterIP pour NGINX** ( si on ne spécifie pas le type, c'est par défaut ClusterIp)
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: nginx-service
    spec:
      selector:
        app: nginx
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
    ```

```bash
kubectl apply -f nginx-service.yaml
```

### 2- Exposer un Pod au réseau interne du cluster

Un Service ClusterIP permet d'exposer un ensemble de Pods à d'autres ressources internes du cluster.

1. **Obtenir l'adresse IP interne du Service**
      
```bash
   kubectl get svc nginx-service -o jsonpath='{.spec.clusterIP}'
```
ou moyennant **minikube**

```bash
    minikube service nginx-service --url
```


### 3- Exposer un Pod à l'extérieur du réseau

Pour permettre l'accès depuis l'extérieur du cluster, vous pouvez utiliser un Service de type NodePort ou LoadBalancer.

1. a) **Créer un Service NodePort pour NGINX**
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: nginx-nodeport
    spec:
      selector:
        app: nginx
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
      type: NodePort
    ```

    ```bash
    kubectl apply -f nginx-nodeport.yaml
    ```

1. b)**Obtenir l'URL pour accéder au service**
    ```bash
    minikube service nginx-nodeport --url
    ```


2. **Créer un Service LoadBalancer pour NGINX**
    ```yaml
   apiVersion: v1
   kind: Service
   metadata:
      name: nginx-loadbalancer-service
   spec:
      selector:
        app: nginx
   ports:
      - protocol: TCP
        port: 80
        targetPort: 80
   type: LoadBalancer
    ```

Exécutez le tunnel dans un terminal séparé.

```bash
minikube tunnel
```

**Créer le service de type LoadBalancer**

```bash
kubectl apply -f nginx-LB.yaml
```

**Obtenir l'URL pour accéder au service**

```bash
minikube service nginx-loadbalancer-service --url
```
