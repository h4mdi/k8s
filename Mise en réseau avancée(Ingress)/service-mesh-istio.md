# Istio ( https://istio.io/latest/docs/examples/bookinfo/)

## Installation: 
- https://github.com/istio/istio/releases/tag/1.22.1
##

```sh
istioctl install --set profile=demo -y
```
Le résultat :

```The default revision has been updated to point to this installation.
This will install the Istio 1.22.1 "default" profile (with components: Istio core, Istiod, and Ingress gateways) into the cluster. Proceed? (y/N) y
✔ Istio core installed
✔ Istiod installed
✔ Ingress gateways installed
✔ Installation complete                                                                                                Made this installation the default for injection and validation.
```
Ajouter un label de namespace pour instruire Istio à injecter automatiquement les proxies sidecar Envoy lorsque vous déploierez votre application plus tard :
```sh
kubectl label namespace default istio-injection=enabled
```

```
namespace/default labeled                                                                                               Made this installation the default for injection and validation.
```

Déployer l'application d'exemple Bookinfo :

```sh
kubectl apply -f bookinfo.yaml
```
L'application va démarrer. À mesure que chaque pod devient prêt, le sidecar Istio sera déployé avec lui.
```sh
kubectl get services
```
```sh
kubectl get pods
```
```sh
kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage
```

Vérifiez que tout fonctionne correctement jusqu'à présent. Exécutez cette commande pour voir si l'application fonctionne à l'intérieur du cluster et sert des pages HTML en vérifiant la sortie de la page dans la réponse :

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Simple Bookstore App</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="static/bootstrap/css/bootstrap-theme.min.css">

  </head>
  <body>



<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BookInfo Sample</a>
    </div>

    <button type="button" class="btn btn-default navbar-btn navbar-right" data-toggle="modal" href="#login-modal">Sign
      in</button>

  </div>
</nav>

<!---
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header pull-left">
      <a class="navbar-brand" href="#">Microservices Fabric BookInfo Demo</a>
    </div>
    <div class="navbar-header pull-right">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse">

      <button type="button" class="btn btn-default navbar-btn pull-right" data-toggle="modal" data-target="#login-modal">Sign in</button>

    </div>
  </div>
</div>
-->

<div id="login-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please sign in</h4>
      </div>
      <div class="modal-body">
        <form method="post" action='login' name="login_form">
          <p><input type="text" class="form-control" name="username" id="username" placeholder="User Name"></p>
          <p><input type="password" class="form-control" name="passwd" placeholder="Password"></p>
          <p>
            <button type="submit" class="btn btn-primary">Sign in</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </p>
        </form>
      </div>
    </div>

  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-center text-primary">The Comedy of Errors</h3>

      <p>Summary: <a href="https://en.wikipedia.org/wiki/The_Comedy_of_Errors">Wikipedia Summary</a>: The Comedy of Errors is one of <b>William Shakespeare's</b> early plays. It is his shortest and one of his most farcical comedies, with a major part of the humour coming from slapstick and mistaken identity, in addition to puns and word play.</p>

    </div>
  </div>

  <div class="row">
    <div class="col-md-6">

      <h4 class="text-center text-primary">Book Details</h4>
      <dl>
        <dt>Type:</dt>paperback
        <dt>Pages:</dt>200
        <dt>Publisher:</dt>PublisherA
        <dt>Language:</dt>English
        <dt>ISBN-10:</dt>1234567890
        <dt>ISBN-13:</dt>123-1234567890
      </dl>

    </div>

    <div class="col-md-6">

      <h4 class="text-center text-primary">Book Reviews</h4>

      <blockquote>
        <p>An extremely entertaining play by Shakespeare. The slapstick humour is refreshing!</p>
        <small>Reviewer1</small>

      </blockquote>

      <blockquote>
        <p>Absolutely fun and entertaining. The play lacks thematic depth when compared to other plays by Shakespeare.</p>
        <small>Reviewer2</small>

      </blockquote>

      <dl>
        <dt>Reviews served by:</dt>
        <u>reviews-v1-7d6b7d799d-86zfw</u>

      </dl>

    </div>
  </div>
</div>



<!-- Latest compiled and minified JavaScript -->
<script src="static/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
  $('#login-modal').on('shown.bs.modal', function () {
    $('#username').focus();
  });
</script>

  </body>
</html>
```

## Ouvrir l'application au trafic extérieur
L'application Bookinfo est déployée mais n'est pas accessible de l'extérieur. Pour la rendre accessible, vous devez créer un Istio Ingress Gateway, qui mappe un chemin vers une route à la périphérie de votre mesh.

1. Associez cette application avec la passerelle Istio :

```sh 
kubectl apply -f bookinfo-gateway.yaml
```
```
gateway.networking.istio.io/bookinfo-gateway created
virtualservice.networking.istio.io/bookinfo created
```

2. Assurez-vous qu'il n'y a pas de problèmes avec la configuration :

```sh
istioctl analyze
```
```
✔ No validation issues found when analyzing namespace: default.
```

## Déterminer l'IP et les ports d'ingress

Suivez ces instructions pour définir les variables INGRESS_HOST et INGRESS_PORT pour accéder à la passerelle. Utilisez les onglets pour choisir les instructions pour votre plateforme choisie :

Pour minikube :
```sh
minikube tunnel
```
accès :

127.0.0.1/productpage


## Tableau de bord :
Istio s'intègre à plusieurs applications de télémétrie différentes. Elles peuvent vous aider à comprendre la structure de votre mesh de service, afficher la topologie du mesh et analyser la santé de votre mesh.

Utilisez les instructions suivantes pour déployer le tableau de bord Kiali, ainsi que Prometheus, Grafana et Jaeger.

Pour minikube :
```sh
kubectl apply -f kiali.yaml
```

```sh
kubectl apply -f prometheus.yaml
```

```sh
kubectl apply -f jaeger.yaml
```

Accédez au tableau de bord Kiali.
```sh
istioctl dashboard kiali
```

Pour voir les données de traçage, vous devez envoyer des requêtes à votre service. Le nombre de requêtes dépend du taux d'échantillonnage d'Istio et peut être configuré en utilisant l'API de télémétrie. Avec le taux d'échantillonnage par défaut de 1%, vous devez envoyer au moins 100 requêtes avant que la première trace ne soit visible. Pour envoyer 100 requêtes au service productpage, utilisez la commande suivante :

```bash
for ($i = 1; $i -le 100; $i++) {
    Invoke-WebRequest -Uri "http://127.0.0.1/productpage" | Out-Null
}
```
<img src="images/kiali.png"/>

Le tableau de bord Kiali montre une vue d'ensemble de votre mesh avec les relations entre les services dans l'application d'exemple Bookinfo. Il fournit également des filtres pour visualiser le flux de trafic.

<img src="images/istio.png"/>

## Request Routing
Cette tâche vous montre comment acheminer dynamiquement les demandes vers plusieurs versions d'un microservice.

L'exemple Bookinfo d'Istio se compose de quatre microservices distincts, chacun avec plusieurs versions. Trois versions différentes de l'un des microservices, reviews, ont été déployées et fonctionnent simultanément. Pour illustrer le problème que cela pose, accédez à la page /productpage de l'application Bookinfo dans un navigateur et actualisez plusieurs fois. L'URL est http://$GATEWAY_URL/productpage, où $GATEWAY_URL est l'adresse IP externe de l'ingress, comme expliqué dans la documentation de Bookinfo.

Vous remarquerez que parfois la sortie des avis de livres contient des évaluations par étoiles et d'autres fois non. Cela est dû au fait que, sans une version de service par défaut explicite à laquelle router, Istio achemine les requêtes vers toutes les versions disponibles de manière circulaire (round robin).

L'objectif initial de cette tâche est d'appliquer des règles qui routent tout le trafic vers v1 (version 1) des microservices. Par la suite, vous appliquerez une règle pour router le trafic en fonction de la valeur d'un en-tête de requête HTTP.


### Définir les versions de service
Avant de pouvoir utiliser Istio pour contrôler le routage des versions de Bookinfo, vous devez définir les versions disponibles. Istio utilise des sous-ensembles, dans les règles de destination, pour définir les versions d'un service. Exécutez la commande suivante pour créer les règles de destination par défaut pour les services Bookinfo :

```bash
kubectl apply -f destination-rule-all.yaml
```

Attendez quelques secondes pour que les règles de destination se propagent.

Vous pouvez afficher les règles de destination avec la commande suivante :

```bash
kubectl get destinationrules -o yaml
```

Pour router vers une seule version, vous devez configurer des règles de routage qui envoient le trafic vers les versions par défaut des microservices.
1. Exécutez la commande suivante pour créer les règles de routage :
   Istio utilise des services virtuels pour définir les règles de routage. Exécutez la commande suivante pour appliquer les services virtuels qui routeront tout le trafic vers v1 de chaque microservice :

```bash
 kubectl apply -f virtual-service-all-v1.yaml
   ```
Comme la propagation de la configuration est éventuellement cohérente, attendez quelques secondes pour que les services virtuels prennent effet.

2. Affichez les routes définies avec la commande suivante :

```bash
kubectl get virtualservices -o yaml
```

Vous pouvez également afficher les définitions des sous-ensembles correspondants avec la commande suivante :

```bash
kubectl get destinationrules -o yaml
```

Vous avez configuré Istio pour router vers la version v1 des microservices Bookinfo, en particulier la version 1 du service reviews.

On peut modifier les versions dans le fichier `virtual-service-all-v1.yaml` en changeant, par exemple, la version de `ratings` de `v1` à `v2`. Un simple rafraîchissement (F5) dans le navigateur affichera la différence entre les deux versions.

3. Route basée sur l'identité de l'utilisateur

Danz cette partie, vous allez modifier la configuration de la route de sorte que tout le trafic provenant d'un utilisateur spécifique soit dirigé vers une version spécifique du service. 
Dans ce cas, tout le trafic de l'utilisateur nommé **Jason** sera dirigé vers le service reviews

Cet exemple est rendu possible par le fait que le service productpage ajoute un en-tête utilisateur final personnalisé à toutes les requêtes HTTP sortantes vers le service reviews.

On peut utiliser le fichier `virtual-service-user-route.yaml`.

Ce manifeste YAML définit une ressource VirtualService dans Istio, utilisée pour gérer le routage du trafic vers différents sous-ensembles d'un service nommé "reviews" en fonction des en-têtes des requêtes HTTP.

Il configure le routage du trafic vers le service "reviews" : les requêtes avec l'en-tête "end-user: jason" sont routées vers "reviews:v2" (version 2 du service), tandis que les autres requêtes sont routées vers "reviews:v1" (version 1 du service).

Sur la page /productpage de l'application Bookinfo, connectez-vous en tant qu'utilisateur Jason.

Actualisez le navigateur. Que voyez-vous ? Les évaluations par étoiles apparaissent à côté de chaque critique.

Connectez-vous en tant qu'un autre utilisateur (choisissez n'importe quel nom).

Actualisez le navigateur. Maintenant, les étoiles ont disparu. Cela est dû au fait que le trafic est dirigé vers reviews:v1 pour tous les utilisateurs sauf Jason.

Vous avez configuré avec succès Istio pour router le trafic en fonction de l'identité de l'utilisateur.
