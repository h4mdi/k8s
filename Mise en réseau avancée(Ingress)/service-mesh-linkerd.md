###wsl ! ou git bash

# Linkerd

La première chose à faire est d'installer l'interface en ligne de commande (CLI) de Linkerd, afin que nous puissions interagir avec les services Linkerd. Pour ce faire, nous exécutons la commande suivante :

https://github.com/linkerd/linkerd2/releases/

Linkerd a été téléchargé avec succès. Ajoutons maintenant le CLI de Linkerd au chemin de notre système d'exploitation en l'exportant comme une variable d'environnement, on peut aussi utiliser le binaire directement:

Confirmons également que le CLI de Linkerd a été installé avec succès sur notre machine locale. Nous faisons cela simplement en exécutant la commande suivante :

```sh
linkerd version
```
ou
```sh
.\linkerd2-cli-edge-24.6.2-windows.exe
```

La sortie devrait être :

```
Client version: edge-24.6.2
Server version: unavailable
```


La commande suivante permet de s'assurer que Linkerd peut être installé sur le cluster:
```
linkerd check --pre
```
```
.\linkerd2-cli-edge-24.6.2-windows.exe check --pre
kubernetes-api
--------------
√ can initialize the client
√ can query the Kubernetes API

kubernetes-version
------------------
√ is running the minimum Kubernetes API version

pre-kubernetes-setup
√ no clock skew detected

linkerd-version
---------------
√ can determine the latest version
√ cli is up-to-date

Status check results are √
```

A l'aide de la commande suivante, installez à présent les composants du control plane de Linkerd:

```
.\linkerd2-cli-edge-24.6.2-windows.exe install --crds | kubectl apply -f -
 .\linkerd2-cli-edge-24.6.2-windows.exe install |  kubectl apply -f -     
 .\linkerd2-cli-edge-24.6.2-windows.exe install --set proxyInit.runAsRoot=true |  kubectl apply -f -

 .\linkerd2-cli-edge-24.6.2-windows.exe check --pre 
```

## Visualisation
Depuis la version 2.10, Linkerd permet d'installer des extensions afin de réduire l'utilisation des ressources. Utilisez la commande suivante afin d'installer l'extension viz qui mettra en place Prometheus, le dashboard Linkerd ainsi que les composants permettant la gestion des metrics:
```
.\linkerd2-cli-edge-24.6.2-windows.exe viz install | kubectl apply -f -
```

Vérifiez une nouvelle fois que tout a bien été installé, notamment dans la section Linkerd extensions checks
```
.\linkerd2-cli-edge-24.6.2-windows.exe check
```
Une fois l'extension viz installé, le dashboard Linkerd est accessible via:
```
.\linkerd2-cli-edge-24.6.2-windows.exe viz dashboard
```

# Demo1: Emojivoto Application

Afin d'illustrer les fonctionnalités de base de Linkerd, vous allez installer l'application emojivoto (application microservices présentée dans la documentation officielle de Linkerd) qui permet de voter entre différents Emojis.

## Déploiement de l'application

https://run.linkerd.io/emojivoto.yml

```sh
kubectl apply -f emojivoto.yml
```
Vous devriez voir une sortie similaire à celle-ci:

```
namespace/emojivoto created
serviceaccount/emoji created
serviceaccount/voting created
serviceaccount/web created
service/emoji-svc created
service/voting-svc created
service/web-svc created
deployment.apps/emoji created
deployment.apps/vote-bot created
deployment.apps/voting created
deployment.apps/web created
```

*Note: un générateur de trafic (vote-bot) est également déployé afin d'envoyer régulièrement des requêtes à l'application.*

## Accès à l'application

Afin d'avoir accès à l'application depuis votre machine locale, utilisez la commande `port-forward` afin d'exposer le service nommé `web-svc`:

```sh
kubectl -n emojivoto port-forward svc/web-svc 8080:80
```

L'application emojivoto est alors disponible à l'adresse [http://localhost:8080](http://localhost:8080). Il est donc possible de voter pour son Emoji favori :)


Remarquez alors que le nouveau namespace `emojivoto` est maintenant disponible depuis l'interface web de Linkerd. L'application n'étant pas encore gérée par Linkerd, aucun de ces services ne sont encore dans le mesh (colonne Meshed de l'interface).


# Gestion de l'application avec Linkerd

Pour intégrer les fonctionnalités de Linkerd à l'application emojivoto, chaque Pod de l'application est modifié pour inclure un nouveau conteneur agissant comme un proxy à l'intérieur de celui-ci. Cette opération peut être effectuée facilement à l'aide de la commande suivante, qui modifie la spécification de tous les Déploiements du namespace emojivoto :

```bash
kubectl get -n emojivoto deploy -o yaml | .\linkerd2-cli-edge-24.6.2-windows.exe inject - | kubectl apply -f -
```

```plaintext
deployment "vote-bot" injecté
deployment "web" injecté
deployment "emoji" injecté
deployment "voting" injecté

deployment.apps/vote-bot configuré
deployment.apps/web configuré
deployment.apps/emoji configuré
deployment.apps/voting configuré
```

Les proxies Linkerd sont maintenant installés dans chaque microservice de l'application. Cela peut être vérifié à l'aide de la commande suivante :

```bash
.\linkerd2-cli-edge-24.6.2-windows.exe -n emojivoto check --proxy
```

```plaintext
kubernetes-api
--------------
√ can initialize the client
√ can query the Kubernetes API

kubernetes-version
------------------
√ is running the minimum Kubernetes API version

linkerd-existence
-----------------
√ 'linkerd-config' config map exists
√ heartbeat ServiceAccount exist
√ control plane replica sets are ready
√ no unschedulable pods
√ control plane pods are ready
√ cluster networks contains all node podCIDRs
√ cluster networks contains all pods
√ cluster networks contains all services

linkerd-config
--------------
√ control plane Namespace exists
√ control plane ClusterRoles exist
√ control plane ClusterRoleBindings exist
√ control plane ServiceAccounts exist
√ control plane CustomResourceDefinitions exist
√ control plane MutatingWebhookConfigurations exist
√ control plane ValidatingWebhookConfigurations exist
√ proxy-init container runs as root user if docker container runtime is used

linkerd-identity
----------------
√ certificate config is valid
√ trust anchors are using supported crypto algorithm
√ trust anchors are within their validity period
√ trust anchors are valid for at least 60 days
√ issuer cert is using supported crypto algorithm
√ issuer cert is within its validity period
√ issuer cert is valid for at least 60 days
√ issuer cert is issued by the trust anchor

linkerd-webhooks-and-apisvc-tls
-------------------------------
√ proxy-injector webhook has valid cert
√ proxy-injector cert is valid for at least 60 days
√ sp-validator webhook has valid cert
√ sp-validator cert is valid for at least 60 days
√ policy-validator webhook has valid cert
√ policy-validator cert is valid for at least 60 days

linkerd-identity-data-plane
---------------------------
√ data plane proxies certificate match CA

linkerd-version
---------------
√ can determine the latest version
√ cli is up-to-date

linkerd-control-plane-proxy
---------------------------
√ control plane proxies are healthy
√ control plane proxies are up-to-date
√ control plane proxies and cli versions match

linkerd-data-plane
------------------
√ data plane namespace exists
√ data plane proxies are ready
√ data plane is up-to-date
√ data plane and cli versions match
√ data plane pod labels are configured correctly
√ data plane service labels are configured correctly
√ data plane service annotations are configured correctly
√ opaque ports are properly annotated

                              linkerd-viz
-----------
√ linkerd-viz Namespace exists
√ can initialize the client
√ linkerd-viz ClusterRoles exist
√ linkerd-viz ClusterRoleBindings exist
√ tap API server has valid cert
√ tap API server cert is valid for at least 60 days
√ tap API service is running
√ linkerd-viz pods are injected
√ viz extension pods are running
√ viz extension proxies are healthy
√ viz extension proxies are up-to-date
√ viz extension proxies and cli versions match
√ prometheus is installed and configured correctly
√ viz extension self-check

linkerd-viz-data-plane
----------------------
√ data plane namespace exists
√ prometheus is authorized to scrape data plane pods
√ data plane proxy metrics are present in Prometheus

Status check results are √

```

En quelques commandes, vous avez donc déployé une application et ajouté celle-ci au Mesh mis en place par Linkerd. Un nouveau container a été ajouté dans chaque Pod de l'application, ce container est un proxy par lequel passe tous les flux entrants et sortants du container applicatif à côté duquel il se trouve. Linkerd permet alors de gérer ce réseau de proxy et notamment d'obtenir des informations sur les flux réseau qui les traversent. Ces informations sont très importantes pour mettre en évidence certains problèmes et aider à les localiser, voire à les résoudre.
A noter que tout ceci a été effectué sans changer le code source de l'application.

# Demo2: Book application

Afin d'illustrer les fonctionnalités plus avancées de Linkerd, vous allez installer l'application bookapp (application microservices présentée dans la documentation officielle de Linkerd) qui sert à gérer un ensemble de livres.
Cette application comporte 3 microservices comme l'illustre le schéma suivant:

<img src="images/book.png"/>

**Note:** un générateur de trafic est également déployé afin d'envoyer régulièrement des requêtes à l'application

## Déploiement de l'application
Utilisez la commande suivante pour créer le namespace bookapp et les différentes ressources de l'application dans ce namespace:

```sh
kubectl create ns booksapp
```
Télécharger les ressource via:

https://run.linkerd.io/booksapp.yml

vérifions que les ressources sont crées:

```sh
kubectl -n booksapp get all
```

Vous devriez obtenir un résultat similaire à celui ci-dessous:
```
NAME                           READY   STATUS              RESTARTS   AGE
pod/authors-79887c5578-zsd5l   0/1     ContainerCreating   0          10s
pod/books-675456b8d-rhc2x      0/1     ContainerCreating   0          9s
pod/traffic-84f4545cdf-qwmqk   0/1     ContainerCreating   0          9s
pod/webapp-5f459f867b-ctkbs    0/1     ContainerCreating   0          10s
pod/webapp-5f459f867b-tzmbf    0/1     ContainerCreating   0          10s
pod/webapp-5f459f867b-xlr4s    0/1     ContainerCreating   0          10s

NAME              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/authors   ClusterIP   10.104.116.187   <none>        7001/TCP   10s
service/books     ClusterIP   10.104.242.68    <none>        7002/TCP   10s
service/webapp    ClusterIP   10.101.100.136   <none>        7000/TCP   10s

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/authors   0/1     1            0           10s
deployment.apps/books     0/1     1            0           9s
deployment.apps/traffic   0/1     1            0           9s
deployment.apps/webapp    0/3     3            0           10s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/authors-79887c5578   1         1         0       10s
replicaset.apps/books-675456b8d      1         1         0       9s
replicaset.apps/traffic-84f4545cdf   1         1         0       9s
replicaset.apps/webapp-5f459f867b    3         3         0       10s
PS C:\Users\h4mdi\Desktop\LEMP\FormationK8S\Mise en réseau avancée(Ingress)> kubectl -n booksapp get all
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/authors   0/1     1            0           60s
deployment.apps/books     0/1     1            0           59s
deployment.apps/traffic   1/1     1            1           59s
deployment.apps/webapp    0/3     3            0           60s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/authors-79887c5578   1         1         0       60s
replicaset.apps/books-675456b8d      1         1         0       59s
replicaset.apps/traffic-84f4545cdf   1         1         1       59s
replicaset.apps/webapp-5f459f867b    3         3         0       60s
```

### Accès à l'application:

Afin d'avoir accès à l'application depuis votre machine locale, utilisez la commande port-forward afin d'exposer le service nommé webapp:

```sh
kubectl -n booksapp port-forward svc/webapp 7000
```

L'application est alors disponible à l'adresse http://localhost:7000.
<img src="images/book_home.png"/>


Si vous essayez d'ajouter un livre, vous obtiendrez cependant régulièrement une erreur:

<img src="images/err_book.png"/>


Depuis l'interface web de Linkerd, on peut également voir que le namespace bookapp a été détecté, mais pour le moment aucun des services n'est présent dans le mesh:

<img src="images/linkerd.png"/>

### Gestion de l'application avec Linkerd
Afin d'ajouter les fonctionnalités de Linkerd à l'application nous allons modifier chaque Pod de l'application de façon à ajouter un nouveau container faisant office de proxy à l'intérieur de chacun d'entre eux, comme nous l'avions fait pour l'application emojivoto. Utilisez pour cela la commande suivante:

```bash
kubectl get -n booksapp deploy -o yaml | .\linkerd2-cli-edge-24.6.2-windows.exe inject - | kubectl apply -f -
```
```
deployment "authors" injected
deployment "books" injected
deployment "traffic" injected
deployment "webapp" injected

deployment.apps/authors configured
deployment.apps/books configured
deployment.apps/traffic configured
deployment.apps/webapp configured
```

Les proxy Linkerd sont maintenant installés dans chaque microservice de l'application. Vous pouvez le vérifier avec la commande suivante:

```sh
.\linkerd2-cli-edge-24.6.2-windows.exe -n booksapp check --proxy
```

Nous pouvons alors voir que les services de l'application sont maintenant présent dans le Mesh:

<img src="images/linkerOk.png"/>

Vous pourrez notamment voir l'évolution des "Success Rates" pour chaque requête envoyée à l'application par le générateur de trafic.
Sélectionnez le deployment webapp afin d'obtenir davantage d'informations.

<img src="images/books_mesh.png"/>
