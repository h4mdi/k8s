### La Ressource DaemonSet

Un DaemonSet assure que tous (ou certains) nœuds exécutent une copie d'un pod. Dès qu'un nœud est ajouté au cluster, un pod est également ajouté pour ce DaemonSet.

#### Exemple de création d'un DaemonSet

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nom-daemonset
spec:
  selector:
    matchLabels:
      name: nom-daemonset
  template:
    metadata:
      labels:
        name: nom-daemonset
    spec:
      containers:
      - name: nom-container
        image: image:tag
        ports:
        - containerPort: 8080
```

Pour appliquer ce DaemonSet :

```bash
kubectl apply -f daemonset.yaml
```




Dans cet exercice, vous allez manipuler un DaemonSet.

Dans le fichier *nginx-ds.yaml*, créez une spécification de DaemonSet avec les caractéristiques suivantes:
- le DaemonSet à le nom *nginx-daemonset*
- chaque Pod a un seul container basé sur l'image *nginx*

Créez le DaemonSet définit dans cette spécification et regardez les logs des Pods créés.

Note: vous pourrez utiliser la documentation suivante pour vous aider: [https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/).

Supprimez ensuite le DaemonSet.
