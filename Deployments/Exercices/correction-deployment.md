## Correction

### 1. Création d'un Deployment

La spécification, définie dans le fichier *ghost_deployment.yaml* est la suivante:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ghost
spec:
  replicas: 3
  selector:
    matchLabels:
      app: ghost
  template:
    metadata:
      labels:
        app: ghost
    spec:
      containers:
      - name: ghost
        image: ghost:4
        ports:
        - containerPort: 2368
```

La commande suivante permet de créer le Deployment

```
kubectl apply -f ghost_deployment.yaml
```

### 2. Status du Deployment

La commande suivante permet d'obtenir le status du Deployment

```
kubectl get deploy
NAME   READY   UP-TO-DATE   AVAILABLE   AGE
ghost   3/3     3            3          51s
```

### 3. Status des Pods associés

La commande suivante permet de lister les Pods qui tournent sur le cluster

```
kubectl get po
NAME                    READY   STATUS    RESTARTS   AGE
ghost-548879c755-7kmpz   1/1     Running   0          68s
ghost-548879c755-m5pjt   1/1     Running   0          68s
ghost-548879c755-nwl9l   1/1     Running   0          68s
```

On voit que les 3 Pods relatifs au Deployment *ghost* sont listés. Ils sont tous les 3 actifs.

### 4. Exposition des Pods du Deployment

Dans un fichier *ghost_service.yaml* nous définissons la spécification suivante:

```
apiVersion: v1
kind: Service
metadata:
  name: ghost
spec:
  selector:
    app: ghost
  type: NodePort
  ports:
  - port: 80
    targetPort: 2368
    nodePort: 31001
```

On crée ensuite le Service:

```
kubectl apply -f ghost_service.yaml
```

### 5. Cleanup

Supprimez le Deployment et le Service:

```
kubectl delete deploy/ghost
kubectl delete svc/ghost
```

