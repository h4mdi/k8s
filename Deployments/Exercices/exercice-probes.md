### Pérenniser son déploiement



Pour pérenniser votre déploiement, vous devez vous assurer que vos applications sont robustes et résilientes.

### Contrôler l'état de ses pods

Les sondes (probes) sont utilisées pour vérifier l'état de vos pods. Kubernetes offre trois types de sondes : `startupProbe`, `livenessProbe`, et `readinessProbe`.

#### StartupProbe

Utilisé pour vérifier si une application s'est bien lancée.

```yaml
startupProbe:
  httpGet:
    path: /sante
    port: 8080
  failureThreshold: 30
  periodSeconds: 10
```

#### LivenessProbe

Utilisé pour vérifier si l'application est toujours en vie.

```yaml
livenessProbe:
  httpGet:
    path: /sante
    port: 8080
  initialDelaySeconds: 3
  periodSeconds: 3
```

#### ReadinessProbe

Utilisé pour vérifier si l'application est prête à accepter du trafic.

```yaml
readinessProbe:
  httpGet:
    path: /pret
    port: 8080
  initialDelaySeconds: 3
  periodSeconds: 3
```

### La notion de Scale-Out

Le scale-out consiste à ajouter des instances de ressources pour répondre à une demande accrue. En Kubernetes, cela signifie souvent augmenter le nombre de réplicas pour un déploiement.

```bash
# Scaler un déploiement à 5 réplicas
kubectl scale deployment <nom-de-deploiement> --replicas=5

# Voir l'état des réplicas
kubectl get deployment <nom-de-deploiement>
```

## Exercice

1. Créer un déploiement avec une image **nginx**.
2. Ajouter des sondes de démarrage, de vivacité et de préparation.
3. Scaler le déploiement à 3 réplicas.

### Étapes

1. Créer le déploiement.

```bash
kubectl create deployment mon-deploiement --image=nginx
```

2. Ajouter les sondes dans le fichier YAML du déploiement.

```yaml
        startupProbe:
          httpGet:
            path: /
            port: 80
          failureThreshold: 30
          periodSeconds: 10
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 3
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 3
          periodSeconds: 3
```

3. Scaler le déploiement à 3 réplicas.

