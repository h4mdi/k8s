## Correction
- La spécification suivante définit le DaemonSet *nginx-daemonset* dont chaque Pod a un seul container basé sur l'image *nginx*:

```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nginx-daemonset
spec:
  selector:
    matchLabels:
      app: nginx-daemonset  
  template:
    metadata:
      labels:
        app: nginx-daemonset
    spec:
      containers:
      - name: nginx-daemonset
        image: nginx
```

Une fois sauvegardée dans le fichier *nginx-ds.yaml* nous pouvons créer ce DaemonSet:

```
kubectl apply -f log-ds.yaml
```

Nous listons les Pods associés:

```
kubectl get pods -l app=log
```

Nous pouvons alors voir les logs de l'un des Pods associés au DaemonSet:

```
kubectl logs ds/nginx-daemonset
```

(Dans un contexte de production, un DaemonSet pourrait être utilisé pour déployer des Pods qui vont lire les logs de chacun des nodes et les envoyer sur une solution de gestion de logs centralisée (telle que ElasticSearch, Splunk, ...))

Vous pouvez ensuite supprimer ce DaemonSet:

```
kubectl delete ds nginx-daemonset
```
