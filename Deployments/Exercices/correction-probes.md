## Correction

1. Création du déploiement avec l'image Nginx.

```bash
kubectl create deployment mon-deploiement --image=nginx
```

2. Ajout des sondes dans le fichier YAML du déploiement.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mon-deploiement
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mon-deploiement
  template:
    metadata:
      labels:
        app: mon-deploiement
    spec:
      containers:
      - name: nginx
        image: nginx
        startupProbe:
          httpGet:
            path: /
            port: 80
          failureThreshold: 30
          periodSeconds: 10
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 3
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 3
          periodSeconds: 3
```

3. Application du fichier YAML.

```bash
kubectl apply -f deployment.yaml
```

4. Scalage du déploiement à 3 réplicas.

```bash
kubectl scale deployment mon-deploiement --replicas=3
```
