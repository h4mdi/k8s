## Commandes pour DaemonSets

1. **Créer un DaemonSet à partir d'un fichier YAML :**

   ```bash
   kubectl apply -f daemonset.yaml
   ```

   Assurez-vous que votre fichier YAML contient tous les paramètres nécessaires comme `apiVersion`, `kind`, `metadata`, `spec`, etc., spécifiquement adaptés pour un DaemonSet.

2. **Afficher tous les DaemonSets dans un namespace :**

   ```bash
   kubectl get daemonsets -n <namespace>
   ```

   Remplacez `<namespace>` par le namespace dans lequel vous souhaitez lister les DaemonSets. Si aucun namespace n'est spécifié, il utilisera le namespace par défaut.

3. **Afficher les pods associés à un DaemonSet :**

   ```bash
   kubectl get pods -l app=<nom_daemonset>
   ```

   Remplacez `<nom_daemonset>` par le nom du DaemonSet pour afficher les pods qui en sont une instance.

4. **Afficher les détails d'un DaemonSet :**

   ```bash
   kubectl describe daemonset <nom_daemonset>
   ```

   Cela affiche des détails plus approfondis sur le DaemonSet, y compris les événements et les conditions actuelles.

5. **Supprimer un DaemonSet :**

   ```bash
   kubectl delete daemonset <nom_daemonset>
   ```

   Cette commande supprime complètement le DaemonSet et tous les pods qui y sont associés.

6. **Mettre à jour un DaemonSet :**

   Pour mettre à jour un DaemonSet, modifiez le fichier YAML correspondant avec les changements nécessaires, puis appliquez à nouveau :

   ```bash
   kubectl apply -f daemonset.yaml
   ```

   Kubernetes gérera automatiquement le processus de mise à jour en remplaçant progressivement les anciens pods par les nouveaux, en fonction de votre stratégie de mise à jour définie dans le DaemonSet.

