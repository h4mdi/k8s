Cette partie couvre les stratégies de déploiement, en se concentrant sur le mécanisme de rolling update (déploiement bleu-vert), la gestion de l'historique des déploiements, l'annotation des changements de version et la mise à jour des images du déploiement NGINX/PHP avec des tests de Rollback.

**1. Déploiement initial :**

Déployez une application NGINX/PHP initiale avec une version spécifique de l'image (par exemple, `nginx:1.19.10` et `php:7.4-fpm`). Utilisez un déploiement Kubernetes pour cela.

```yaml
# nginx-php-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-php
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-php
  template:
    metadata:
      labels:
        app: nginx-php
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.10
        ports:
        - containerPort: 80
      - name: php
        image: php:7.4-fpm
        ports:
        - containerPort: 9000
```

**2. Effectuer une mise à jour :**

- Modifiez la spécification de l'image dans le fichier de déploiement pour utiliser une nouvelle version (par exemple, `nginx:1.20.1` et `php:7.4-fpm`).
- Appliquez cette mise à jour avec la commande `kubectl apply -f deployment.yaml`.

**3. Observer le déploiement :**

- Vérifiez l'état du déploiement en utilisant la commande `kubectl rollout status deployment nginx-php`.
- Affichez l'historique des déploiements avec `kubectl rollout history deployment nginx-php`.

**4. Ajouter des annotations :**

Ajoutez une annotation indiquant la cause du changement à la spécification du déploiement.

```yaml
# deployment.yaml
...
metadata:
  annotations:
    deployment.kubernetes.io/revision: "2"
    kubernetes.io/change-cause: "Mise à jour vers nginx:1.20.1 et php:7.4-fpm"
```

**5. Tester le Rollback :**

- Si la mise à jour ne fonctionne pas comme prévu, utilisez la commande `kubectl rollout undo deployment nginx-php` pour revenir à la version précédente.
- Vérifiez que le Rollback s'est bien déroulé en observant le statut du déploiement.
