En Kubernetes, les probes sont des vérifications périodiques que l'orchestrateur effectue sur les conteneurs pour s'assurer qu'ils fonctionnent correctement. Voici une explication des trois types de probes :

1. **Startup Probe**: utilisée pour indiquer si l'application dans le conteneur a démarré correctement. Elle est souvent utilisée pour les applications qui ont des temps de démarrage longs.
2. **Liveness Probe**: détermine si l'application dans le conteneur est toujours en vie. Si cette probe échoue, Kubernetes tue le conteneur et tente de le redémarrer en fonction de la politique de redémarrage spécifiée.
3. **Readiness Probe**: vérifie si le conteneur est prêt à recevoir du trafic réseau. Si cette probe échoue, le conteneur est retiré des services qui dirigent le trafic vers lui.


Exemple:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mon-deploiement
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mon-deploiement
  template:
    metadata:
      labels:
        app: mon-deploiement
    spec:
      containers:
        - name: nginx
          image: nginx
          startupProbe:
            httpGet:
              path: /
              port: 80
            failureThreshold: 30
            periodSeconds: 10
          livenessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 3
            periodSeconds: 3
          readinessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 3
            periodSeconds: 3
```

On applique ce déploiement, puis il suffit de taper:
```bash
kubectl get po 
```

Puis de voir le pod qui correspond au déploiement **mon-deploiement**.

```bash
kubectl describe po <mon-deploiement-pod-xxxx> 
```

Voici ce que vous devez rechercher et analyser :

### Examen des Liveness Probes

1. **Liveness Probe Configuration**:
    ```yaml
    Liveness: http-get http://:15020/app-health/nginx/livez delay=3s timeout=1s period=3s #success=1 #failure=3
    ```

    - **Type**: HTTP GET
    - **URL**: `http://:15020/app-health/nginx/livez`
    - **Initial Delay**: 3s
    - **Timeout**: 1s
    - **Period**: 3s
    - **Success Threshold**: 1
    - **Failure Threshold**: 3

2. **Current State of the Container**:
    ```yaml
    Containers:
      nginx:
        State: Running
        Started: Thu, 20 Jun 2024 15:33:29 +0200
        Ready: True
        Restart Count: 0
    ```

   Le conteneur `nginx` est actuellement en cours d'exécution et prêt, sans redémarrages signalés. Cela signifie que la liveness probe est actuellement réussie.

3. **Conditions du Pod**:
    ```yaml
    Conditions:
      Type              Status
      Initialized       True
      Ready             True
      ContainersReady   True
      PodScheduled      True
    ```

   Toutes les conditions sont `True`, ce qui indique que le pod est en bon état général.

4. **Événements**:
    ```yaml
    Events: <none>
    ```

   Aucun événement n'est rapporté, ce qui signifie qu'il n'y a pas eu de problèmes ou d'échecs récents pour ce pod.

### Étapes pour Vérifier les Probes

1. **Observer les événements du pod** :
   Utilisez `kubectl describe pod <nom-du-pod>` pour vérifier les événements récents. Les échecs de liveness probe et les redémarrages associés seront listés ici.

2. **Vérifier les journaux des conteneurs** :
   Utilisez `kubectl logs <nom-du-pod> <nom-du-conteneur>` pour consulter les journaux et diagnostiquer d'éventuels problèmes.

   ```sh
   kubectl logs mon-deploiement-857fbbdd4-jws94 nginx
   ```

3. **Tester manuellement l'URL de la liveness probe** :
   Vous pouvez exécuter un curl à l'intérieur d'un pod ou d'un conteneur pour tester manuellement l'URL configurée dans la liveness probe.

   ```sh
   kubectl exec -it <nom-du-pod> -- curl -v http://localhost:15020/app-health/nginx/livez
   ```

4. **Observer les redémarrages du pod** :
   Si des redémarrages se produisent, c'est souvent une indication que la liveness probe échoue régulièrement. Utilisez `kubectl get pods` pour voir si le nombre de redémarrages (`RESTARTS`) augmente.
