# Commandes Kubernetes pour le Déploiement


## Déploiement d'une Application
Créer un déploiement avec l'image spécifiée:
```sh
kubectl create deployment <nom-du-deploiement> --image=<image>
```

Exemple:
```sh
kubectl create deployment nginx-deployment --image=nginx
```


## Lister les Déploiements
```sh
kubectl get deployments
```

Lister les déploiements dans un namespace spécifique:
```sh
kubectl get deployments -n <nom-du-namespace>
```

## Décrire un Déploiement
Obtenir des détails sur un déploiement:
```sh
kubectl describe deployment <nom-du-deploiement>
```

## Supprimer un Déploiement
```sh
kubectl delete deployment <nom-du-deploiement>
```

Voir les logs d'un pod dans un déploiement:
```sh
kubectl logs deployment/<nom-du-deploiement>
```

## Mise à Jour d'un Déploiement
Mettre à jour l'image de déploiement:
```sh
kubectl set image deployment/<nom-du-deploiement> <nom-du-conteneur>=<nouvelle-image>
```

Exemple:
```sh
kubectl set image deployment/nginx-deployment nginx=nginx:latest
```

## Rouler une Mise à Jour
Voir le statut des mises à jour:
```sh
kubectl rollout status deployment/<nom-du-deploiement>
```


Voir l'historique des mises à jour

```sh
kubectl rollout history deployment/<nom-du-deploiement>
```

Annuler une mise à jour:
```sh
kubectl rollout undo deployment/<nom-du-deploiement>
```



