**1. Structure de base du projet :**

Créez une structure de projet avec des répertoires pour les configurations de base et les overlays pour chaque environnement.

```bash
mkdir kustomize
cd kustomize
mkdir -p base
mkdir -p overlays/dev
mkdir -p overlays/preprod
mkdir -p overlays/prod
```

**2. Déploiement de base :**

Créez un déploiement et un service de base dans `base/deployment.yaml` et `base/service.yaml`.

```yaml
# base/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 80
```

```yaml
# base/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - port: 80
      targetPort: 80
```
Pour tirer parti de Kustomize, nous l'utiliserons pour déployer notre application dans trois environnements : dev, staging, et prod. Kustomize nous permettra de modifier facilement le rendu HTML de Nginx ainsi que les ports utilisés, ce qui facilitera la gestion des configurations spécifiques à chaque environnement.

```yaml
# base/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: index-html-configmap
data:
  index.html: |
    <html>
    <head><title>Welcome to nginx!</title></head>
    <body>
    <h1>Welcome to nginx!</h1>
    </body>
    </html>
```

**3. Configuration des overlays:

- 1. Overlay pour environnement dev

Créez un overlay pour l'environnement dev avec des patches spécifiques.

```yaml
# overlays/dev/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../../base
namespace: dev
patches:
  - path: deployment-patch.yaml
    target:
      kind: Deployment
      name: nginx-deployment
  - path: service-patch.yaml
    target:
      kind: Service
      name: nginx-service
  - path: configmap-patch.yaml  # Assurez-vous que le nom du fichier et le chemin sont corrects
    target:
      kind: ConfigMap
      name: index-html-configmap
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: dev
spec:
  replicas: 2
  template:
    spec:
      containers:
        - name: nginx
          ports:
            - containerPort: 8084
          volumeMounts:
            - name: nginx-index-file
              mountPath: /usr/share/nginx/html/
      volumes:
        - name: nginx-index-file
          configMap:
            name: index-html-configmap
```

```yaml
# service-patch.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: dev
spec:
  ports:
    - name: staging
      port: 8080
      targetPort: 8084
  type: NodePort
```

```yaml
# overlays/dev/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: index-html-configmap
  namespace: dev
data:
  index.html: |
    <html>
    <head><title>Welcome to nginx!</title></head>
    <body>
    <h1>Hello from dev!</h1>
    </body>
    </html>
```

- 2. Overlay pour environnement staging

Créez un overlay pour l'environnement staging avec des patches spécifiques.

```yaml
# overlays/staging/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../../base
namespace: staging
patches:
  - path: deployment-patch.yaml
    target:
      kind: Deployment
      name: nginx-deployment
  - path: service-patch.yaml
    target:
      kind: Service
      name: nginx-service
  - path: configmap-patch.yaml  # Assurez-vous que le nom du fichier et le chemin sont corrects
    target:
      kind: ConfigMap
      name: index-html-configmap
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: staging
spec:
  replicas: 3
  template:
    spec:
      containers:
        - name: nginx
          ports:
            - containerPort: 8082
          volumeMounts:
            - name: nginx-index-file
              mountPath: /usr/share/nginx/html/
      volumes:
        - name: nginx-index-file
          configMap:
            name: index-html-configmap
```

```yaml
# service-patch.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: staging
spec:
  ports:
    - name: staging
      port: 8080
      targetPort: 8082
  type: NodePort
```

```yaml
# overlays/staging/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: index-html-configmap
  namespace: staging
data:
  index.html: |
    <html>
    <head><title>Welcome to nginx!</title></head>
    <body>
    <h1>Hello from staging!</h1>
    </body>
    </html>
```

- 3. Overlay pour environnement prod

Créez un overlay pour l'environnement prod avec des patches spécifiques.

```yaml
# overlays/prod/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../../base
namespace: prod
patches:
  - path: deployment-patch.yaml
    target:
      kind: Deployment
      name: nginx-deployment
  - path: service-patch.yaml
    target:
      kind: Service
      name: nginx-service
  - path: configmap-patch.yaml  # Assurez-vous que le nom du fichier et le chemin sont corrects
    target:
      kind: ConfigMap
      name: index-html-configmap
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: prod
spec:
  replicas: 4  # augmenter le nombre de replicas pour l'env de prod
  template:
    spec:
      containers:
        - name: nginx
          ports:
            - containerPort: 8083
          volumeMounts:
            - name: nginx-index-file
              mountPath: /usr/share/nginx/html/
      volumes:
        - name: nginx-index-file
          configMap:
            name: index-html-configmap
```

```yaml
# service-patch.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: prod
spec:
  ports:
    - name: prod
      port: 8080
      targetPort: 8083
  type: NodePort
```

```yaml
# overlays/staging/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: index-html-configmap
  namespace: prod
data:
  index.html: |
    <html>
    <head><title>Welcome to nginx!</title></head>
    <body>
    <h1>Hello from prod!</h1>
    </body>
    </html>
```



**4. Génération des configurations :**

Utilisez Kustomize pour générer la configuration finale pour les environnement.

```bash
kubectl kustomize overlays/dev
```

```bash
kubectl kustomize overlays/staging
```

```bash
kubectl kustomize overlays/prod
```
Cela va produire la configuration Kubernetes complète qui peut être inspectée ou appliquée avec `kubectl apply -k overlays/<env>`.

**5. Déploiement :**
```bash
kubectl apply -k .\overlays\dev
```


```bash
kubectl apply -k .\overlays\staging
```


```bash
kubectl apply -k .\overlays\prod
```

**6. Cleanup :**
```bash
kubectl delete -k overlays/dev
kubectl delete -k overlays/staging
kubectl delete -k overlays/prod
```

