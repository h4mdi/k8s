## Guide de déploiement avec Kustomize

Dans cet exercice, vous allez configurer et déployer une application Nginx à l'aide de Kustomize, en utilisant différents overlays pour différents environnements (dev, staging, prod).

### 1. Structure de base du projet

Créez une structure de projet avec des répertoires pour les configurations de base et les overlays pour chaque environnement.
Avant de commencer, créez les 3 namespaces 'dev', 'staging' et 'prod'.
### 2. Déploiement de base

Créez un déploiement et un service de base pour nginx dans `base/deployment.yaml` et `base/service.yaml` ayant les spécificités suivantes :

**Deployment**: 

- Image: nginx:latest
- Nom du déploiement: nginx-deployment
- Réplicas: 1
- Sélecteur des pods: Correspond aux labels app: nginx
- Port exposé par le conteneur: 80


**Service** :

- Nom du service: `nginx-service`
- Sélecteur des pods: Sélectionne les pods avec le label `app: nginx`
- Port exposé par le service: 80
- Port cible des pods: 80

Créez également un ConfigMap de base pour l'index HTML de Nginx.

- Nom du ConfigMap: index-html-configmap
  - Données:
      ```html
          <html>
          <head><title>Welcome to nginx!</title></head>
          <body>
          <h1>Welcome to nginx!</h1>
          </body>
          </html>
      ```

Créez le fichier `kustomization.yaml` dans le répertoire `base` qui réference les ressources déja définis.

### 3. Overlay pour environnement dev

Créez un overlay pour l'environnement dev avec les patches spécifiques suivants:

**Spécifications du déploiement Kubernetes** :

- Nom du déploiement: `nginx-deployment`
- Namespace: `dev`
- Réplicas: 2
- Conteneur :
    - Nom: `nginx`
    - Port du conteneur: 8084
    - Volume monté :
        - Nom: `nginx-index-file`
        - Chemin de montage: `/usr/share/nginx/html/`
- Volume :
    - Nom: `nginx-index-file`
    - Source: ConfigMap nommé `index-html-configmap`


Crééz un fichier **kustomization.yaml** sous overlays/dev ayant les spécification suivantes:
- apiVersion: kustomize.config.k8s.io/v1beta1
- type: Kustomization
- Resources:
    - Utilisation des ressources définies dans le répertoire `../../base`.
    - Namespace: `dev`
- Patches:
    - Patch du déploiement (`deployment-patch.yaml`) ciblant le déploiement nommé `nginx-deployment`.
    - Patch du service (`service-patch.yaml`) ciblant le service nommé `nginx-service`.
    - Patch du ConfigMap (`configmap-patch.yaml`) ciblant le ConfigMap nommé `index-html-configmap`.

```yaml
# overlays/dev/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../../base
namespace: dev
patches:
  - path: deployment-patch.yaml
    target:
      kind: Deployment
      name: nginx-deployment
  - path: service-patch.yaml
    target:
      kind: Service
      name: nginx-service
  - path: configmap-patch.yaml
    target:
      kind: ConfigMap
      name: index-html-configmap
```

Cela permet de référencer la configuration de base, de modifier le namespace, et de configurer les chemins des patches à appliquer.

Créez les fichiers de patch pour l'environnement dev.

```yaml
# overlays/dev/deployment-patch.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: dev
spec:
  replicas: 2
  template:
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 8082
          volumeMounts:
            - name: nginx-index-file
              mountPath: /usr/share/nginx/html/
      volumes:
        - name: nginx-index-file
          configMap:
            name: index-html-configmap
```

```yaml
# overlays/dev/service-patch.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: dev
spec:
  ports:
    - name: dev
      port: 8080
      targetPort: 8082
  type: NodePort
```

```yaml
# overlays/dev/configmap-patch.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: index-html-configmap
  namespace: dev
data:
  index.html: |
    <html>
    <head><title>Welcome to nginx!</title></head>
    <body>
    <h1>Hello from dev!</h1>
    </body>
    </html>
```
Faites la même configuration pour les deux autres environnements restants (staging et prod), en appliquant les spécifications suivantes :

**staging** :
- deployment:
    - namespace: staging
    - nombre de replicas: 3
    - containerPort: 8082
- service:
    - nom: nginx-service
    - namespace: staging
    - ports:
        - name: staging
        - targetPort: 8082
- configmap:
    - namespace: staging
    - index.html: |
      ```html
      <html>
      <head><title>Welcome to nginx!</title></head>
      <body>
      <h1>Hello from Staging!</h1>
      </body>
      </html>
      ```

**prod** :
- deployment:
    - namespace: prod
    - nombre de replicas: 4
    - containerPort: 8083
- service:
    - nom: nginx-service
    - namespace: prod
    - ports:
        - name: prod
        - targetPort: 8083
- configmap:
    - namespace: prod
    - index.html: |
      ```html
      <html>
      <head><title>Welcome to nginx!</title></head>
      <body>
      <h1>Hello from Prod!</h1>
      </body>
      </html>
      ```

### 4. Génération des configurations

Utilisez Kustomize pour générer la configuration finale pour chaque environnement.


### 5. Déploiement

Déployez l'application dans chaque environnement.

### 6. Cleanup

Supprimez les ressources créées.
