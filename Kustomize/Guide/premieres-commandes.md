### Commandes de base

1. **Afficher la configuration générée**:
   ```bash
   kubectl kustomize <chemin_vers_votre_kustomization_directory>
   ```
    - Cette commande affiche la configuration Kubernetes générée à partir des fichiers kustomization dans le répertoire spécifié.

2. **Appliquer directement à Kubernetes**:
   ```bash
   kubectl apply -k <chemin_vers_votre_kustomization_directory>
   ```
