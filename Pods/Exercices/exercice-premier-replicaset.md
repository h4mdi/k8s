# Création d'un Replicaset

## Exercice

Dans cet exercice, vous allez créer une spécification pour lancer un ReplicaSet dans un cluster Kubernetes. 
Le ReplicaSet devra garantir que trois instances d'un serveur web Nginx sont toujours disponibles. 

Vous devrez rédiger un fichier YAML décrivant les spécifications du ReplicaSet, en incluant les détails tels que le nom, le nombre de réplicas, et l'image Docker à utiliser. Ensuite, vous allez déployer ce ReplicaSet dans le cluster Kubernetes. 

Une fois déployé, vous vérifierez que le ReplicaSet et les pods sont opérationnels.

### 1. Création de la spécification

Créez un fichier yaml *replicaset-example.yaml* définissant un Replicaset ayant les propriétés suivantes:
- nom du Replicaset: *nginx-replicaset*
- nombre de replicas: *3*
- image du container: *nginx:latest*
- nom du container: *nginx*
- port: *80*

### 2. Déployer de ReplicaSet

Créez le ReplicaSet à l'aide de *kubectl*

### 3. Vérification de statut du ReplicaSet et des pods

Listez les replicasets et les pods présents et assurez vous que le RS *nginx-replicaset* apparait bien dans cette liste.


### 4. Simulez une défaillance en supprimant un pod

Utilisez la commande *delete pod nginx-replicaset-xxxxx* pour supprimer le pod 'nginx-replicaset-xxxxx' et observez la réaction du ReplicaSet.


### 5. Suppression du Pod

Supprimez le replicaset.

