## Correction

### 1. Création

Les commandes suivantes permettent de créer les namespaces *development* et *production*.

```
kubectl create namespace development

kubectl create namespace production
```

### 2. Liste des namespaces

La commande suivante permet de lister les namespaces présents sur le système.

```
kubectl get namespace
```

### 3. Création de pods

La commande suivante permet de créer le pod *www-0* dans le namespace par default

```
kubectl run www-0 --image nginx:1.24-alpine
```

La commande suivante permet de créer le pod *www-1* dans le namespace *development*

```
kubectl run www-1 --image nginx:1.24-alpine --namespace development
```

La commande suivante permet de créer le pod *www-2* dans le namespace *production*

```
kubectl run www-2 --image nginx:1.24-alpine --namespace production
```

### 4. Répartition des ressources

La commande suivante permet de lister l'ensemble des Pods dans tous les namespaces:

```
kubectl get po --all-namespaces
```

### 5. Suppression

La commande suivante permet de supprimer les namespaces *development* et *production*:

```
kubectl delete ns development production
```

Si nous listons les Pods, nous pouvons voir que seuls les resources créées dans le namespace *default* sont présentes. Les ressources des namespaces *development* et *production* ont été supprimées avec la suppression de ces 2 namespaces.

La commande suivante permet de supprimer le pod *www-0* existant dans le namespace *default*:

```
kubectl delete deploy www-0
```
