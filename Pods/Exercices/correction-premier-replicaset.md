### Correction


1. **Création d'un ReplicaSet**:

Créez un fichier YAML nommé replicaset-example.yaml avec le contenu suivant :

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
   name: nginx-replicaset
spec:
   replicas: 3
   selector:
      matchLabels:
         app: nginx
   template:
      metadata:
         labels:
            app: nginx
      spec:
         containers:
            - name: nginx
              image: nginx:latest
              ports:
                 - containerPort: 80
   ```
 
2. **Déployer le ReplicaSet** :

   Lors de l'application du fichier YAML avec `kubectl apply -f replicaset-example.yaml`, vous devriez voir un message indiquant que le ReplicaSet a été créé :

   ```
   replicaset.apps/nginx-replicaset created
   ```

3. **Vérifiez le statut du ReplicaSet et des pods** :

   La commande `kubectl get replicaset` ou `kubectl get rs` devrait montrer que le ReplicaSet a le nombre désiré de réplicas :

   ```
   NAME               DESIRED   CURRENT   READY   AGE
   nginx-replicaset   3         3         3       1m
   ```

   La commande `kubectl get pods` devrait montrer trois pods en cours d'exécution :

   ```bash
   NAME                     READY   STATUS    RESTARTS   AGE
   nginx-replicaset-xxxxx   1/1     Running   0          1m
   nginx-replicaset-xxxxx   1/1     Running   0          1m
   nginx-replicaset-xxxxx   1/1     Running   0          1m
   ```

4. a) **Simulez une défaillance en supprimant un pod** :

   Supposons que l'un des pods s'appelle `nginx-replicaset-abcde`. Vous pouvez le supprimer avec :

   ```bash
   kubectl delete pod nginx-replicaset-abcde
   ```

   Vous verrez un message indiquant que le pod est en cours de suppression :

   ```
   pod "nginx-replicaset-abcde" deleted
   ```

4. b) **Observez la réaction du ReplicaSet** :

   Après avoir supprimé le pod, utilisez la commande `kubectl get pods` pour vérifier que le ReplicaSet a créé un nouveau pod pour remplacer celui qui a été supprimé :

   ```bash
   NAME                     READY   STATUS    RESTARTS   AGE
   nginx-replicaset-xxxxx   1/1     Running   0          30s
   nginx-replicaset-xxxxx   1/1     Running   0          1m
   nginx-replicaset-yyyyy   1/1     Running   0          5s
   ```


Vous devriez voir un nouveau pod avec un nom différent de celui supprimé, et tous les pods devraient être en statut `Running`.

5. **Suppression du ReplicaSett** :

   ```bash
   kubectl delete rs nginx-replicaset
   ```
