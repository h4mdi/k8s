# Déployer et Gérer un Pod avec `kubectl`

## Déployer le Pod

Pour déployer votre Pod, utilisez la commande suivante :

``` bash
kubectl apply -f my-first-pod.yaml
```

## Vérifier l'état du Pod

``` bash
kubectl get pods
```

## Obtenir les détails du Pod déployé

```bash
kubectl describe pod my-first-pod
```

## Examiner les logs du Pod
```bash
kubectl logs my-first-pod
```

## Lancement d'une commande dans un Pod existant
```bash
kubectl exec my-first-pod -v
```
## Suppression d'un Pod
```bash
Kubectl delete pod my-first-pod
```
## Shell intéractif dans un POD
```bash
kubectl exec -ti my-first-pod /bin/bash
```
