Quelques bonnes pratiques incluent : utiliser des labels pour organiser les ressources, limiter les ressources des conteneurs, et utiliser des namespaces pour isoler les environnements.

## 1- Ajouter des limites de ressources à un Pod

``` bash
apiVersion: v1
kind: Pod
metadata:
  name: resource-limited-pod
  labels:
    app: nginx
spec:
  containers:
    - name: nginx
      image: nginx:latest
      resources:
        limits:
          memory: "128Mi"
          cpu: "500m"
      ports:
        - containerPort: 80
```


### Déployer et vérifier

``` bash
kubectl apply -f resource-limited-pod.yaml
kubectl describe pod resource-limited-pod
```

## 2- Organiser la gestion des Pods
Les labels, sélecteurs et namespaces permettent d'organiser et de gérer efficacement les ressources.
### a- Utiliser des labels et des sélecteurs

Les Labels et Selectors sont la méthode standard pour regrouper des éléments dans Kubernetes. Nous pouvons filtrer les objets selon des critères tels que la classe, le type et les fonctions.

- Les Labels sont les propriétés attachées à chaque élément/objet.
- Les Selectors nous aident à filtrer les éléments/objets qui ont des labels attachés à eux.

Nous avons un nombre différent de types d'objets dans Kubernetes, comme les Pods, ReplicaSet, Deployments, Services, etc.

Au fil du temps, lorsque notre infrastructure croît, nous aurons des milliers d'objets dans notre cluster.

Et il y a des situations où vous avez besoin d'un moyen pour filtrer et visualiser différents objets par différentes catégories. Par exemple, grouper les objets par leurs types ou par application ou par leur fonctionnalité, peu importe.

Pour travailler avec les Labels et les Selectors, tout ce que vous avez à faire est d'attacher des labels selon vos besoins au format clé-valeur pour chaque objet.

Dans un fichier de définition de pod sous metadata, créez une section appelée labels. Ajoutez-y les labels au format clé-valeur comme :
```yaml
app: app1
type: frontend
```

Ensuite, lors de la sélection, spécifiez une condition pour filtrer des objets spécifiques, c'est-à-dire type=frontend.

Nous pouvons ajouter autant de labels que nous le souhaitons à un Pod. Une fois le Pod créé, pour sélectionner le pod avec des labels, utilisez la commande `kubectl get pods` avec l'option selector et spécifiez la condition comme :

```sh
kubectl get pods --selector type=frontend
```

Ceci est un cas d'utilisation des labels et selectors. Les objets Kubernetes utilisent également les labels et selectors en interne pour travailler avec différents objets ensemble.

Exemple:

``` yaml
apiVersion: v1
kind: Pod
metadata:
  name: labeled-pod
  labels:
    app: nginx
    environment: production
spec:
  containers:
  - name: nginx
    image: nginx:latest
    ports:
    - containerPort: 80
```
``` yaml
apiVersion: v1
kind: Service
metadata:
    name: nginx-nodeport-service
spec:
    type: NodePort
    selector:
        app: nginx
        environment: production
    ports:
        - protocol: TCP
          port: 80
          targetPort: 80
          nodePort: 30007  # Choisissez un port dans la plage 30000-32767
```

Dans Kubernetes, les labels et les selectors sont des concepts essentiels utilisés pour identifier, regrouper et sélectionner des objets au sein du cluster. Voici une explication détaillée en utilisant vos exemples de configuration YAML :

### Labels

Les labels sont des paires clé-valeur attachées aux objets Kubernetes tels que les pods, les services, les deployments, etc. Ils sont utilisés pour identifier et organiser les ressources de manière arbitraire et peuvent être utilisés pour diverses fins telles que le regroupement, le filtrage et la sélection.

Exemple :
```yaml
metadata:
  labels:
    app: nginx
    environment: production
```

- Dans cet exemple, le pod est étiqueté avec deux labels : `app: nginx` et `environment: production`.
- Ces labels sont des métadonnées qui n'affectent pas directement le fonctionnement du pod mais permettent de le catégoriser selon l'application (`app: nginx`) et l'environnement (`environment: production`).

### Selectors

Les selectors sont utilisés pour identifier les objets Kubernetes qui correspondent à certains critères définis par des labels. Les objets Kubernetes comme les services utilisent des selectors pour spécifier quels pods ils doivent cibler pour leur fonctionnement.

Exemple :
```yaml
spec:
  selector:
    app: nginx
    environment: production
```
- Dans cet exemple de service, le selector spécifie `app: nginx` et `environment: production`.
- Cela signifie que ce service va cibler tous les pods qui ont ces deux labels spécifiques (`app: nginx` et `environment: production`).

### Utilisation dans l'exemple donné

Dans notre exemple :

1. **Pod avec Labels** :
   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: labeled-pod
     labels:
       app: nginx
       environment: production
   spec:
     containers:
     - name: nginx
       image: nginx:latest
       ports:
       - containerPort: 80
   ```
    - Le pod nommé `labeled-pod` est étiqueté avec les labels `app: nginx` et `environment: production`.
    - Ces labels peuvent être utilisés pour sélectionner ce pod lors de la définition d'autres objets Kubernetes comme les services.

2. **Service avec Selector** :
   ```yaml
   apiVersion: v1
   kind: Service
   metadata:
       name: nginx-nodeport-service
   spec:
       type: NodePort
       selector:
           app: nginx
           environment: production
       ports:
           - protocol: TCP
             port: 80
             targetPort: 80
             nodePort: 30007
   ```
    - Le service `nginx-nodeport-service` est configuré pour cibler les pods qui ont les labels `app: nginx` et `environment: production`.
    - Le selector `app: nginx` et `environment: production` garantit que seuls les pods avec ces labels seront pris en compte par ce service.

#### Déployer et vérifier
``` bash
kubectl apply -f labeled-pod.yaml
kubectl get pods -l environment=production
```
```
NAME          READY   STATUS    RESTARTS   AGE
labeled-pod   2/2     Running   0          21s
```

### a- Utiliser des namespaces
#### Créer un namespace et déployer un Pod dans ce namespace
``` bash
kubectl create namespace my-namespace
```

#### Utiliser le namespace dans un pod
``` bash

apiVersion: v1
kind: Pod
metadata:
name: namespaced-pod
namespace: my-namespace
labels:
app: nginx
spec:
containers:
- name: nginx
  image: nginx:latest
  ports:
    - containerPort: 80
```

#### Déployer et vérifier
``` bash
kubectl apply -f namespaced-pod.yaml
kubectl get pods -n my-namespace
```


## 4- Le cycle de vie des pods
Le cycle de vie d'un Pod comprend plusieurs phases : Pending, Running, Succeeded, Failed, et Unknown.
### a- Observer le cycle de vie d'un Pod
``` bash
kubectl get pod my-first-pod -o jsonpath='{.status.phase}'
```
### b- Pour voir toutes les étapes

``` bash
kubectl get pod my-first-pod -o jsonpath='{.status.containerStatuses[0].state}'
```
