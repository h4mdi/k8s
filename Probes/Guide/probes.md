
https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/


# Explication:

# Création d'un déploiement de base NGINX

Maintenant, créons un déploiement de base NGINX sans aucun contrôle de santé. Enregistrez le contenu suivant sous le nom 'basic-deployment.yaml' :


Ensuite, déployons cette configuration en utilisant `kubectl apply -f basic-deployment.yml` et vérifions que tous les pods sont dans un état :

```bash
kubectl apply -f basic-deployment.yml
```
```
deployment.apps/nginx créé
```
```
kubectl get pods
```
```
NAME                    READY   STATUS    RESTARTS   AGE
nginx-7f456874f4-fmtwv   1/1     Running   0          23s
nginx-ff6774dc6-wrsjj   1/1     Running   0          23s
```

Comme nous pouvons le voir, maintenant tous les pods sont en état de fonctionnement.

# Comprendre le besoin des sondes Kubernetes

Dans l'étape précédente, nous avons créé un déploiement basique de NGINX sans aucun contrôle de santé. Cependant, par défaut, Kubernetes fournit une vérification de santé du processus, qui vérifie si le processus principal du conteneur est en cours d'exécution. Si ce n'est pas le cas, alors par défaut, Kubernetes redémarre ce conteneur.

De plus, nous utilisons l'objet de déploiement de Kubernetes pour exécuter plusieurs instances du Pod, dans ce cas 2. Peut-on dire que ce déploiement est robuste et résilient aux échecs ? Pas vraiment. Voyons pourquoi.

Tout d'abord, vérifions que le serveur web NGINX est sain et capable de rendre la page d'accueil. Assurez-vous de remplacer l'un des noms de pod de votre cluster, car ils seront différents :

```bash
kubectl exec -it nginx-7f456874f4-fmtwv -- curl http://localhost
```

Si tout est correct, la commande ci-dessus affichera la page d'accueil HTML par défaut de NGINX dans le terminal.

Le serveur web NGINX rend sa page d'accueil à partir du fichier `/usr/share/nginx/html/index.html`. Maintenant, pour simuler un scénario d'erreur, supprimons ce fichier sur le conteneur et exécutons la même requête HTTP à l'aide de la commande curl :

```bash
kubectl exec -it nginx-7f456874f4-fmtwv -- rm /usr/share/nginx/html/index.html

kubectl exec -it nginx-7f456874f4-fmtwv -- curl http://localhost
```

Dans la sortie ci-dessus, nous pouvons voir que nous obtenons maintenant une erreur 403 Forbidden.

Ici, nous pouvons remarquer que même si le démon NGINX est en cours d'exécution, il ne remplit pas de fonction utile à ce moment-là. Parce qu'il ne peut pas rendre la page requise, il renvoie le code d'état HTTP 403.

Il est tout à fait possible que toute autre application web se retrouve dans une situation similaire. Un tel scénario est que la machine virtuelle Java (JVM) peut générer l'erreur OutOfMemoryError, mais le processus JVM est toujours en cours d'exécution. C'est une situation problématique car l'application ne peut pas répondre aux demandes, mais la vérification de santé du processus la considère comme saine.

Dans de tels scénarios, la solution rapide et à court terme est de redémarrer le Pod. Ne serait-il pas formidable que cela se produise automatiquement ? En fait, nous pouvons y parvenir en utilisant les sondes Kubernetes. Apprenons donc à les connaître davantage.

# Types de sondes Kubernetes

Surveiller la santé d'une application est une tâche essentielle. Cependant, la surveillance seule ne suffit pas et nous devons prendre des mesures correctives en cas de défaillances pour maintenir la disponibilité globale du système. Kubernetes fournit un moyen fiable d'y parvenir en utilisant des sondes. Il propose les trois types de sondes suivants :

**Sonde de vivacité (Liveness probe)** : Cette sonde vérifie en permanence si le conteneur est sain et fonctionnel. S'il détecte un problème, alors par défaut, il redémarre le conteneur.

**Sonde de disponibilité (Readiness probe)** : Cette sonde vérifie si le conteneur est prêt à accepter les requêtes entrantes. Si oui, les requêtes sont envoyées au conteneur pour traitement ultérieur.

**Sonde de démarrage (Startup probe)** : Cette sonde détermine si un conteneur a démarré ou non.

Chaque sonde fournit trois méthodes différentes pour vérifier la santé de l'application :

- **Commande (Command)** : Cette méthode exécute la commande fournie dans un conteneur. Tant que la valeur de retour est 0, c'est-à-dire pas d'erreur, cela indique un succès.

- **TCP** : Cette méthode tente d'établir une connexion TCP avec le conteneur. L'établissement réussi de la connexion indique un succès.

- **Requête HTTP** : Cette méthode exécute une requête HTTP sur le conteneur. Un code d'état HTTP de réponse entre 200 et 399 (inclus) indique un succès.

Nous venons de discuter des types de sondes et de leurs méthodes. Mais laquelle devrions-nous utiliser ? Il n'y a pas de réponse unique fixe, car cela dépend de votre application. Nous pouvons choisir la méthode la plus adaptée à l'application. C'est la raison pour laquelle les différents types de sondes existent.

## Sonde de vivacité (Liveness probe)

Dans la section précédente, nous avons vu que la vérification de santé du processus ne pouvait pas reconnaître si l'application est fonctionnelle bien qu'elle semble en vie. Parfois, redémarrer l'application peut résoudre le problème intermittent. Dans de tels cas, nous pouvons utiliser la sonde de vivacité de Kubernetes.

La sonde de vivacité nous permet de définir une vérification de santé spécifique à l'application. En d'autres termes, ce mécanisme fournit un moyen fiable de surveiller la santé de toute application donnée. Comprenons son utilisation avec un exemple.

### Définition d'une commande de vivacité
Définissons la sonde de vivacité pour vérifier l'existence du fichier `/usr/share/nginx/html/index.html`. Nous pouvons utiliser la commande `ls` pour y parvenir. Après avoir ajouté la sonde de vivacité, notre définition de déploiement de l'exemple précédent ressemble à ceci :

**liveness.yaml** :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  namespace: probe-demo
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 80
          livenessProbe:
            exec:
              command:
                - ls
                - /usr/share/nginx/html/index.html
```

Maintenant, déployons cette configuration mise à jour et vérifions que les pods sont dans un état sain :

```bash
kubectl apply -f liveness.yaml
```
```
deployment.apps/nginx configuré
```
```
kubectl get pods
```
```
NAME                     READY   STATUS    RESTARTS   AGE
nginx-cfcfc9d87-64944   1/1     Running   0          42s
nginx-847c64cc7c-jb2bq   1/1     Running   0          46s
```

Ensuite, supprimons le fichier `/usr/share/nginx/html/index.html` du pod et observons ses événements. Assurez-vous une fois de plus de substituer le nom du pod par celui de `kubectl get pods` sur votre cluster :

```bash
kubectl exec -it nginx-cfcfc9d87-64944 -- rm /usr/share/nginx/html/index.html
```

```
kubectl get event --field-selector involvedObject.name=nginx-cfcfc9d87-64944
```
```
LAST SEEN   TYPE      REASON      OBJECT                       MESSAGE
3m21s       Normal    Scheduled   pod/nginx-cfcfc9d87-64944   Successfully assigned probe-demo/nginx-cfcfc9d87-64944 to proble-demo-control-plane
0s          Normal    Pulling     pod/nginx-cfcfc9d87-64944   Pulling image "nginx"
3m18s       Normal    Pulled      pod/nginx-cfcfc9d87-64944   Successfully pulled image "nginx" in 2.19317793s
3m18s       Normal    Created     pod/nginx-cfcfc9d87-64944   Created container nginx
3m18s       Normal    Started     pod/nginx-cfcfc9d87-64944   Started container nginx
1s          Warning   Unhealthy   pod/nginx-cfcfc9d87-64944   Liveness probe failed: ls: cannot access '/usr/share/nginx/html/index.html': No such file or directory
1s          Normal    Killing     pod/nginx-cfcfc9d87-64944   Container nginx failed liveness probe, will be restarted
```

Dans la sortie ci-dessus, nous pouvons voir que Kubernetes a marqué le pod comme non sain (unhealthy) et l'a redémarré. Nous pouvons voir ces détails respectivement dans les colonnes RAISON (REASON) et MESSAGE.

Enfin, vérifions que le pod a bien été redémarré :

```bash
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-cfcfc9d87-64944   1/1     En cours d'exécution   1 (Il y a 42s)   4m2s
nginx-cfcfc9d87-64944   1/1     En cours d'exécution   0             4m6s
```

Dans la sortie ci-dessus, la colonne REDÉMARRAGES indique que le pod a été redémarré il y a 42 secondes.


## Définition d'une sonde de vivacité TCP
Similairement aux sondes de commande, nous pouvons utiliser la sonde de socket TCP pour vérifier la santé de l'application. Comme son nom l'indique, cette sonde tente d'établir une connexion TCP avec le conteneur sur un port spécifié. La sonde est considérée comme réussie si la connexion est établie avec succès.

Actuellement, le serveur NGINX fonctionne sur le port 80. Pour simuler une erreur, essayons de nous connecter au port 8080 en utilisant la sonde TCP suivante :

```yaml
livenessProbe:
  tcpSocket:
    port: 8080
```

Après avoir ajouté cette configuration de sonde, le descripteur de déploiement ressemble à ceci :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        livenessProbe:
          tcpSocket:
            port: 8080
```

Déployons cette configuration mise à jour et vérifions les événements du pod :

```bash
kubectl apply -f tcp-liveness.yaml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-795d87cffb-ng6fp   1/1     En cours d'exécution   0             28s
nginx-5bb9d87b58-rfnlh   1/1     En cours d'exécution   0             21s
```

```
kubectl get event  --field-selector involvedObject.name=nginx-795d87cffb-ng6fp
DERNIÈRE VUE   TYPE      RAISON      OBJET                       MESSAGE
33s            Normal    Scheduled   pod/nginx-795d87cffb-ng6fp   Assignation réussie de pod/nginx-5bb9d87b58-cwpzf à proble-demo-control-plane
3s             Normal    Pulling     pod/nginx-795d87cffb-ng6fp   Extraction en cours de l'image "nginx"
27s            Normal    Pulled      pod/nginx-795d87cffb-ng6fp   Extraction réussie de l'image "nginx" en 5.719997947s
1s             Normal    Created     pod/nginx-795d87cffb-ng6fp   Création du conteneur nginx
0s             Normal    Started     pod/nginx-795d87cffb-ng6fp   Démarrage du conteneur nginx
3s             Warning   Unhealthy   pod/nginx-795d87cffb-ng6fp   Échec de la sonde de vivacité : composition de la connexion refusée tcp 10.244.0.7:8080: connexion : connexion refusée
3s             Normal    Killing     pod/nginx-795d87cffb-ng6fp   Échec de la sonde de vivacité du conteneur nginx, redémarré
1s             Normal    Pulled      pod/nginx-795d87cffb-ng6fp   Extraction réussie de l'image "nginx" en 1.928558648s
```

Dans la sortie ci-dessus, nous pouvons voir que la sonde de vivacité a échoué car la connexion a été refusée sur le port 8080. Pour résoudre ce problème, nous pouvons corriger la sonde de vivacité pour utiliser le port 80, où le serveur est en écoute.

## Définition d'une requête HTTP de vivacité
De nombreuses applications Web exposent un point de terminaison HTTP pour signaler l'état de santé de l'application. Par exemple, dans le framework Spring Boot Actuator, nous pouvons utiliser le point de terminaison actuator/health pour vérifier l'état de l'application. Voyons maintenant comment configurer un point de terminaison HTTP dans une sonde de vivacité.

Par défaut, le serveur NGINX rend la page d'accueil à l'URL de base. Pour simuler une erreur, essayons d'accéder à un point de terminaison HTTP inexistant en utilisant la sonde suivante :

```yaml
livenessProbe:
  httpGet:
    path: /non-existing-endpoint
    port: 80
```

Après avoir ajouté la configuration de la sonde, le descripteur complet du déploiement ressemble à ceci :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /non-existing-endpoint
            port: 80
```

Déployons cette configuration et vérifions les événements du pod. Assurez-vous d'utiliser le nom de pod de votre cluster plutôt que le nom d'exemple ci-dessous :

```bash
kubectl apply -f http-liveness.yaml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-5ddf9b649-rqhm2   1/1     En cours d'exécution   0             16s
nginx-7c459d7c6c-vmlq5   1/1     En cours d'exécution   0             11s
```
```
kubectl get event --field-selector involvedObject.name=nginx-5ddf9b649-rqhm2
```
```
LAST SEEN   TYPE      REASON      OBJECT                       MESSAGE
30s         Normal    Scheduled   pod/nginx-5ddf9b649-rqhm2   Successfully assigned probe-demo/nginx-7c459d7c6c-tlqp5 to proble-demo-control-plane
30s         Normal    Pulling     pod/nginx-5ddf9b649-rqhm2   Pulling image "nginx"
27s         Normal    Pulled      pod/nginx-5ddf9b649-rqhm2   Successfully pulled image "nginx" in 3.58879558s
27s         Normal    Created     pod/nginx-5ddf9b649-rqhm2   Created container nginx
27s         Normal    Started     pod/nginx-5ddf9b649-rqhm2   Started container nginx
1s          Warning   Unhealthy   pod/nginx-5ddf9b649-rqhm2   Liveness probe failed: HTTP probe failed with statuscode: 404
1s          Normal    Killing     pod/nginx-5ddf9b649-rqhm2   Container nginx failed liveness probe, will be restarted```
```

Ici, nous pouvons voir que la sonde de vivacité a échoué comme prévu avec le code d'état HTTP 404. Pour résoudre ce problème, nous pouvons utiliser un point de terminaison HTTP valide (comme /) avec la sonde de vivacité.

Il est important de noter que la sonde de vivacité n'est pas une solution universelle. Elle joue un rôle précieux uniquement si votre application peut se permettre de redémarrer les pods affectés, et si le redémarrage peut résoudre les problèmes intermittents de l'application. Elle ne corrigera pas les erreurs de configuration ou les bogues dans le code de votre application.

# Sonde de disponibilité**

Dans la section précédente, nous avons vu comment la sonde de vivacité nous permet d'implémenter un système d'auto-guérison dans certaines situations. Cependant, d'après notre expérience pratique, nous savons que dans la plupart des cas, avoir uniquement une sonde de vivacité n'est pas suffisant.

La sonde de vivacité permet de redémarrer les conteneurs non sains. Cependant, dans certains cas rares, le conteneur peut ne pas être sain dès le départ, et le redémarrer ne sera d'aucune aide. 

Un exemple de tel scénario est lorsque nous tentons de déployer une nouvelle version de l'application qui n'est pas saine. Illustrons cela avec un exemple.

**Rectification de la configuration**

Dans la section précédente, nous avons déployé un pod non sain pour illustrer l'échec de la sonde de vivacité HTTP. Maintenant, modifions-le pour utiliser le point de terminaison HTTP valide. Voici à quoi ressemble le descripteur de déploiement modifié :

**http-liveness.yaml :**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
```

Déployons cette configuration et vérifions que les pods sont dans un état sain :

```bash
kubectl apply -f http-liveness.yaml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-9bc69459d-d99nx   1/1     Running   0             104m
nginx-5bb954fdcb-wsfjm   1/1     Running   0             104m
```

**Rupture de la sonde de vivacité**

Précédemment, nous avons vu que la sonde de vivacité joue un rôle crucial lorsque l'application déployée est saine mais qu'elle devient ultérieurement non saine. 

Cependant, la sonde de vivacité ne pourra pas faire grand-chose si l'application n'est pas saine dès le départ.

Pour simuler un scénario d'application non saine, configurons le hook postStart qui supprime le fichier index.html dans /usr/share/nginx/html :

**breaking-liveness.yml :**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
        lifecycle:
          postStart:
            exec:
              command: ["/bin/bash", "-c", "rm -f /usr/share/nginx/html/index.html"]
```
Dès que le conteneur défini dans le déploiement démarre, le hook postStart exécutera la commande définie dans le cadre de la mise en place du pod.

Déployons maintenant cette configuration et observons le comportement des pods nouvellement déployés :

```bash
kubectl apply -f breaking-liveness.yml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES     ÂGE
nginx-756d44d4fc-lgzvz   1/1     Running   4 (il y a 3s)   2m4s
nginx-76fb56d59f-kx24d   1/1     Running   4 (il y a 6s)   2m6s
```

Comme nous pouvons le voir, maintenant les pods redémarrent continuellement. **Un tel scénario peut entraîner une indisponibilité en production**. Dans la prochaine section, nous discuterons de la manière d'éviter de tels comportements indésirables.

Avant de passer à la section suivante, revenons en arrière en déployant la configuration du fichier `http-liveness.yaml` précédent :

```bash
kubectl apply -f http-liveness.yaml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-9bc69459d-ntpf7   1/1     Running   0             4m23s
nginx-5bb954fdcb-xmhlr   1/1     Running   0             4m26s
```

**Définition de la sonde de disponibilité HTTP**

Dans l'exemple précédent, nous avons vu comment une application non saine peut entraîner une indisponibilité en production. Nous pouvons atténuer de tels échecs en configurant une sonde de disponibilité. 

La syntaxe de la sonde de disponibilité est similaire à celle des sondes de vivacité :

**http-readiness.yml :**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
        lifecycle:
          postStart:
            exec:
              command: ["/bin/bash", "-c", "rm -f /usr/share/nginx/html/index.html"]
        readinessProbe:
          httpGet:
            path: /
            port: 80
```

Déployons cette configuration et observons l'état du nouveau pod créé :

```bash
kubectl apply -f http-readiness.yml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NAME                     READY   STATUS    RESTARTS   AGE
nginx-79999d6487-tgf9j   1/2     Running   0          29s
nginx-9bc69459d-jmzqk    2/2     Running   0          63s
nginx-9bc69459d-qdzcr    2/2     Running   0          69s

```

Dans la sortie ci-dessus, nous pouvons voir qu'il y a maintenant trois pods. Mais la colonne READY est particulièrement importante.

Pour le dernier pod, nous pouvons voir que la colonne READY indique 1/2. Cela indique que 1 sur 2 pods sont prêts à recevoir le trafic entrant. Pour cette raison, le nouveau pod est considéré comme non sain. Par conséquent, Kubernetes ne supprime pas les anciens pods. De cette manière, nous pouvons utiliser une combinaison de sondes de vivacité et de disponibilité pour garantir que seuls les conteneurs sains servent les requêtes entrantes.

Enfin, nous pouvons supprimer la section postStart erronée dans le déploiement pour rendre le déploiement sain.

Dans cette section, nous avons illustré l'utilisation de la sonde de disponibilité uniquement avec la sonde HTTP. Cependant, nous pouvons également utiliser les méthodes de sonde de commande et de TCP pour configurer la sonde de disponibilité. Leur syntaxe est similaire à celle des sondes de vivacité correspondantes.

# Sonde de démarrage

Kubernetes fournit également la sonde de démarrage. Cependant, cette sonde n'est pas aussi connue que les deux autres. Elle est principalement utilisée avec une application qui prend du temps pour démarrer. Lorsque la sonde de démarrage est configurée, elle désactive les deux autres sondes jusqu'à ce que cette sonde réussisse. Cela empêche les erreurs ou les alertes des sondes de vivacité ou d'autres sondes de déclencher inutilement.

La syntaxe de la sonde de démarrage est similaire à celle des autres sondes :
```
startupProbe:
httpGet:
path: /
port: 80
```
Pour comprendre son utilisation, créons un déploiement non sain avec la sonde de démarrage. Le déploiement suivant supprime la page par défaut index.html de NGINX mais définit également une sonde de disponibilité qui produirait une erreur car la page qu'elle tente d'accéder n'est pas disponible :

**http-startup.yaml :**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  namespace: probe-demo
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 80
          livenessProbe:
            httpGet:
              path: /
              port: 80
          lifecycle:
            postStart:
              exec:
                command: ["/bin/bash", "-c", "rm -f /usr/share/nginx/html/index.html"]
          readinessProbe:
            httpGet:
              path: /
              port: 80
          startupProbe:
            httpGet:
              path: /
              port: 80
  ```

Déployons cette configuration et vérifions que la sonde de démarrage désactive les deux autres sondes :

```bash
kubectl apply -f http-startup.yaml
```
```
deployment.apps/nginx configured
```
```
kubectl get pods
```
```
NOM                      PRÊT    STATUT    REDÉMARRAGES   ÂGE
nginx-5bb954fdcb-2fgm2   1/1     Running   0             35m
nginx-5bb954fdcb-xmhlr   1/1     Running   0             35m
nginx-5f4576574c-dqhfp   0/1     Running   0             24s
```
```
kubectl get event --field-selector involvedObject.name=nginx-6c74fc495c-9l6d8
```
```
LAST SEEN   TYPE      REASON      OBJECT                       MESSAGE
95s         Normal    Scheduled   pod/nginx-6c74fc495c-9l6d8   Successfully assigned probe-demo/nginx-5f4576574c-dqhfp to proble-demo-control-plane
5s          Normal    Pulling     pod/nginx-6c74fc495c-9l6d8  Pulling image "nginx"
93s         Normal    Pulled      pod/nginx-6c74fc495c-9l6d8   Successfully pulled image "nginx" in 2.046922256s
33s         Normal    Created     pod/nginx-6c74fc495c-9l6d8  Created container nginx
33s         Normal    Started     pod/nginx-6c74fc495c-9l6d8   Started container nginx
5s          Warning   Unhealthy   pod/nginx-6c74fc495c-9l6d8   Startup probe failed: HTTP probe failed with statuscode: 403
5s          Normal    Killing     pod/nginx-6c74fc495c-9l6d8   Container nginx failed startup probe, will be restarted```
```

Dans la sortie ci-dessus, nous pouvons voir que le pod a été marqué comme non sain car la sonde de démarrage a échoué.

Pour rendre la configuration fonctionnelle à nouveau, nous pouvons supprimer la section postStart.

Tout comme les sondes de vivacité, nous pouvons également utiliser les méthodes de sonde de commande et de TCP pour configurer les sondes de démarrage.

**Configuration avancée de la sonde**

Jusqu'à présent, nous avons utilisé les sondes dans leur configuration par défaut. En réalité, une solution pour une défaillance de la sonde consiste à redémarrer le pod a été mentionnée. Cependant, nous pouvons annuler cela en fonction des besoins de l'application. Chaque paramètre de configuration est décrit en détail dans le tableau ci-dessous :

| Paramètre              | Description                                                            | Valeur par défaut | Valeur minimale |
|------------------------|------------------------------------------------------------------------|-------------------|-----------------|
| initialDelaySeconds    | Durée après le démarrage du conteneur mais avant le démarrage des sondes | 0                 | 0               |
| periodSeconds          | Fréquence de la sonde                                                  | 10                | 1               |
| timeoutSeconds         | Délai d'attente pour les réponses de la sonde                           | 1                 | 1               |
| successThreshold       | Nombre minimum de réponses successives consécutives pour marquer la sonde comme réussie | 1                 | 1               |
| failureThreshold       | Nombre minimum de réponses échouées consécutives pour marquer la sonde comme échouée | 1                 | 1               |

