Dans cette partie, nous allons déployer une application NGINX/PHP avec une configuration personnalisée pour NGINX. Nous allons utiliser des ConfigMaps pour gérer les configurations et des Secrets pour stocker des informations sensibles.

### 1- Utilisation des Secrets

Les Secrets sont utilisés pour stocker des informations sensibles telles que des mots de passe, des jetons OAuth, et des clés SSH. Ils sont similaires aux ConfigMaps mais offrent une meilleure sécurité en chiffrant les données.

1. Créez un fichier pour le Secret (`secret.yml`) :

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-secret
type: Opaque
data:
  db-username: cGhwLXVzZXI=   # Encodé en base64 (php-user)
  db-password: c2VjcmV0UGFzc3dvcmQ=  # Encodé en base64 (secretPassword)
```

2. Appliquez le Secret à votre cluster Kubernetes :

```bash
kubectl apply -f secret.yml
```


### 2- Utilisation des ConfigMaps

Les ConfigMaps sont utilisés pour stocker des configurations non sensibles sous forme de paires clé-valeur. Ils permettent de décorréler les configurations de l'application de l'image conteneur.

1. Créez un fichier de configuration NGINX personnalisé :

Nous allons remplacer la configuration par défaut de NGINX par une configuration personnalisée pour adapter le serveur web à nos besoins spécifiques. 

La configuration par défaut de NGINX est conçue pour un usage général et ne convient pas toujours à des cas spécifiques comme l'intégration de PHP. En remplaçant cette configuration, nous pouvons :

- Adapter le serveur pour écouter sur un port spécifique (8091 dans notre cas).
- Définir des paramètres personnalisés pour la gestion des fichiers PHP.
```nginx
    events {}
    http {
      server {
        listen 8091;
        index index.html index.htm index.php;
        root  /var/www/html;
        location ~ \.php$ {
          include fastcgi_params;
          fastcgi_param REQUEST_METHOD $request_method;
          fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
          fastcgi_pass 127.0.0.1:9000;
        }
      }
    }

```


2. Créez un ConfigMap pour stocker cette configuration :

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
data:
  nginx.conf: |
    events {}
    http {
      server {
        listen 8091;
        index index.html index.htm index.php;
        root  /var/www/html;
        location ~ \.php$ {
          include fastcgi_params;
          fastcgi_param REQUEST_METHOD $request_method;
          fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
          fastcgi_pass 127.0.0.1:9000;
        }
      }
    }
```

3. Appliquez le ConfigMap à votre cluster Kubernetes :

```bash
kubectl apply -f cm.yaml
```

### 3- Déploiement de NGINX/PHP avec ConfigMap et Secret

Nous allons déployer une application utilisant NGINX et PHP-FPM, configurée via ConfigMap et sécurisée par Secret.

1. Créez un fichier YAML pour le pod (`nginx-php-pod.yaml`) :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-phpfpm
  labels:
    app: nginx-phpfpm
    tier: back-end
spec:
  volumes:
    - name: shared-files
      emptyDir: {}
    - name: nginx-config-volume
      configMap:
        name: nginx-config
  containers:
    - name: nginx-container
      image: nginx:latest
      volumeMounts:
        - name: shared-files
          mountPath: /var/www/html
        - name: nginx-config-volume
          mountPath: /etc/nginx/nginx.conf
          subPath: nginx.conf
    - name: php-fpm-container
      image: php:7.3-fpm
      volumeMounts:
        - name: shared-files
          mountPath: /var/www/html
      env:
        - name: DB_USERNAME
          valueFrom:
            secretKeyRef:
              name: db-secret
              key: db-username
        - name: DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: db-secret
              key: db-password

```

2. Appliquez le déploiement :

```bash
kubectl apply -f nginx-php-pod-default.yaml
```

3. Créez un service pour exposer le déploiement NGINX/PHP (`nginx-php-service.yaml`) :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-phpfpm
spec:
  type: NodePort
  selector:
    app: nginx-phpfpm
    tier: back-end
  ports:
    - port: 8091
      targetPort: 8091
      nodePort: 30012
```

4. Appliquez le service à votre cluster Kubernetes :

```bash
kubectl apply -f nginx-php-service.yaml
```

5. Tester :

Nous allons créer un simple fichier index.php pour notre application PHP. Ce fichier affichera un message "Hello World" et les informations de configuration PHP. Voici le contenu du fichier index.php :

   ```php
  <html>
    <head>
        <title>PHP Hello World!</title>
    </head>
    <body>
        <?php echo '<h1>Hello World</h1>'; ?>
        <?php phpinfo(); ?>
    </body>
</html>
   ```

Vérifiez si le pod est en cours d'exécution :
   ```sh
   kubectl get po
   ```

Si le pod est en cours d'exécution, copiez le fichier `index.php` dans le conteneur `nginx-container` :
   ```sh
   kubectl cp index.php nginx-phpfpm:/var/www/html/ --container=nginx-container
   ```

