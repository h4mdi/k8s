# Création et utilisation d'un Secret

## Exercice

Dans cet exercice vous allez utiliser un Secret pour vous connecter à une base de données externe.

### 1. Le context

L'image *registry.gitlab.com/lucj/messages:v1.0.5* contient une application simple qui écoute sur le port 3000 et permet, via des requêtes HTTP, de créer des messages ou de lister les messages existants dans une base de données *MongoDB*. L'URL de connexion de cette base doit être fournie à l'application de façon à ce que celle-ci puisse s'y connecter. Nous pouvons lui fournir via une variable d'environnement MONGODB_URL ou via un fichier qui devra être accessible depuis */app/db/mongodb_url*.

### 2. La base de données

Pour cet exercice nous allons utiliser la base de données Mongo dont l'URL de connexion est la suivante:

```
mongodb+srv://k8sExercice:k8sExercice@techwhale.hg5mrf8.mongodb.net/
```

Cette database est hostée sur [MongoDB Atlas](https://www.mongodb.com/atlas/database).

### 3. Création du Secret

Créez un Secret nommé *mongo*, le champ *data* de celui-ci doit contenir la clé *mongo_url* dont la valeur est la chaine de connection spécifiée ci-dessus.

'mongodb+srv://k8sExercice:k8sExercice@techwhale.hg5mrf8.mongodb.net/'


### 4. Utilisation du Secret dans une variable d'environnement

Définissez un Pod nommé *messages-env* dont l'unique container a la spécification suivante:

- image: *registry.gitlab.com/lucj/messages:v1.0.5*
- une variable d'environnement *MONGODB_URL* ayant la valeur liée à la clé *mongo_url* du Secret *mongo* créé précédemment

Créez ensuite ce Pod et exposez le en utilisant la commande `kubectl port-forward` en faisant en sorte que le port 3000 de votre machine locale soit mappé sur le port 3000 du Pod *messages-env*.

Depuis un autre terminal, vérifiez que vous pouvez créer un message avec la commande suivante:

Note: assurez vous de remplacer *YOUR_NAME* par votre prénom

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from YOUR_NAME"}' http://localhost:3000/messages
```

### 5. Utilisation du Secret dans un volume

Définissez un Pod nommé *messages-vol* ayant la spécification suivante:

- un volume nommé *mongo-creds* basé sur le Secret *mongo*
- un container ayant la spécification suivante:
    - image: *registry.gitlab.com/lucj/messages:v1.0.5*
    - une instructions *volumeMounts* permettant de monter la clé *mongo_url* du volume *mongo*mongo-creds* dans le fichier */app/db/mongo_url*

Créez le Pod et vérifier que vous pouvez créer un message de la même façon que dans le point précédent en exposant le Pod via un *port-forward*.

### 6. Cleanup

Supprimez les différentes resources créées.

---
