
## Correction

### 3. Création du Secret
utilisation d'un fichier de spécification

La première étape est d'encrypter en base64 la chaine de connexion

https://www.base64decode.org/

```
'mongodb+srv://k8sExercice:k8sExercice@techwhale.hg5mrf8.mongodb.net/'

en bae64: bW9uZ29kYitzcnY6Ly9rOHNFeGVyY2ljZTprOHNFeGVyY2ljZUB0ZWNod2hhbGUuaGc1bXJmOC5tb25nb2RiLm5ldC8=
```

Ensuite nous pouvons définir le fichier de spécification mongo-secret.yaml:

```
apiVersion: v1
kind: Secret
metadata:
  name: mongo
data:
  mongo_url: bW9uZ29kYitzcnY6Ly9rOHNFeGVyY2ljZTprOHNFeGVyY2ljZUB0ZWNod2hhbGUuaGc1bXJmOC5tb25nb2RiLm5ldC8=
```

La dernière étape consiste à créer le Secret à partir de ce fichier

```
kubectl apply -f mongo-secret.yaml
```

### 4. Utilisation du Secret dans une variable d'environnement

Nous définissons la spécification suivante dans le fichier *messages-env.yaml*

```
apiVersion: v1
kind: Pod
metadata:
  name: messages-env
spec:
  containers:
  - name: messages
    image: registry.gitlab.com/lucj/messages:v1.0.5
    env:
    - name: MONGODB_URL
      valueFrom:
        secretKeyRef:
          name: mongo
          key: mongo_url
```

Nous pouvons alors créer le Pod:

```
kubectl apply -f messages-env.yaml
```

La commande suivante permet d'exposer en localhost l'API tournant dans le container du Pod:

```
kubectl port-forward messages-env 3000:3000
```

Depuis un autre terminal de la machine locale, nous pouvons alors envoyer une requête POST sur l'API:

Note: assurez vous de remplacer *YOUR_NAME* par votre prénom ( sur windows utiliser git bash)

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from YOUR_NAME"}' http://localhost:3000/messages
```

La réponse retournée est similaire à celle ci-dessous:

```
{"msg":"hello from USER_NAME","created_at":"2023-08-02T11:37:29.796Z"}
```

Nous pouvons ensuite arrêter le port-forward.

### 5. Utilisation du Secret dans un volume

Nous définissons la spécification suivante dans le fichier *messages-vol.yaml*

```
apiVersion: v1
kind: Pod
metadata:
  name: messages-vol
spec:
  containers:
  - name: messages
    image: registry.gitlab.com/lucj/messages:v1.0.5
    volumeMounts:
    - name: mongo-creds
      mountPath: "/app/db"
      readOnly: true
  volumes:
  - name: mongo-creds
    secret:
      secretName: mongo
```

:warning: si vous avez donné un autre nom que *mongo_url* à la clé du Secret (vous l'avez par exemple nommée *mongo*), vous pouvez mettre à disposition cette clé avec la configuration suivante:

```
  volumeMounts:
    - name: mongo-creds
      mountPath: "/app/db/mongo_url"
      subPath: "mongo"
```

Nous pouvons alors créer le Pod:

```
kubectl apply -f messages-vol.yaml
```

La commande suivante permet d'exposer en localhost l'API tournant dans le container du Pod:

```
kubectl port-forward messages-vol 3000:3000
```

Depuis la machine locale, nous pouvons alors envoyer une requête POST sur l'API:

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from USER_NAME"}' http://localhost:3000/messages
```

Nous obtenons alors une réponse simimaire à la suivante:

```
{"msg":"hello from USER_NAME","created_at":"2023-08-02T11:40:26.765Z"}
```

Nous pouvons ensuite arrêter le port-forward.

### 6. Cleanup

```
kubectl delete po messages-env messages-vol
kubectl delete secret mongo
```
