# Mise a jour d'une ConfigMap

Dans cet exercice nous allons créer une ConfigMap et l'utiliser dans un Deployment, nous verrons alors comment faire pour déclencher une mise à jour du Deployment lorsque l'on modifie cette ConfigMap.

## Création d'un configMap

Créez le fichier *nginx.conf* avec le contenu suivant:

```
user nginx;
worker_processes 4;
pid /run/nginx.pid;
events {
   worker_connections 768;
}
http {
  server {
    listen 8080;
    root /usr/share/nginx/html;
  }
}
```

Cette configuration est très simple, elle spécifie simplement que le serveur nginx que nous allons utiliser écoutera sur le port *8080*

A partir de ce fichier, créez une ConfigMap.


## Création d'un Deployment

Créez un fichier *deploy.yaml* qui décrit un déploiement nommé *www* avec les caractéristiques suivantes :
- Sélecteur de Pod avec les labels correspondants à *app: www*
- Modèle de Pod avec les labels *app: www* et un container nommé *nginx* utilisant l'image *nginx:1.14-alpine*
- Un volume nommé *config* monté dans le chemin */etc/nginx/* du container, utilisant une ConfigMap nommée *www-config*.
 
Maintenant, assurez que le serveur web est bien opérationnel sur le port 8080.


## Mise a jour de la configmap

Vous allez à présent mettre la ConfigMap à jour et changer le port d'écoute de *8080* à *9090*. 
Pour cela, utilisez l'une des 2 options suivantes:

### Option 1

Modifiez le fichier *nginx.conf* en remplaçant le port d'écoute par 9090 puis mettez la ConfigMap à jour.

### Option 2

Modifier la ConfigMap "on the fly" depuis un éditeur de texte.


## Impact sur le Deployment

Vérifier que le Pod a été redéployé et qu'il écoute maintenant sur le port 9090.

Regarder la configuration qui est montée dans le container nginx.

## Modification du Deployment
Modifiez le déploiement nommé *www* pour intégrer une stratégie de mise à jour de la ConfigMap utilisée. Voici les étapes à suivre :
- Ajoutez une variable d'environnement nommée *CONFIG_HASH* au container *nginx* du déploiement.
- Cette variable *CONFIG_HASH* doit contenir un hash de la ConfigMap *www-config*, mis à jour à chaque modification de la configuration.
- Assurez-vous que le déploiement conserve les autres spécifications intactes, y compris le volume monté et l'image utilisée pour le container.
- Mettez le fichier *deploy.yaml* à jour avec le contenu ci-dessus et appliquer ces changements au Deployment.
