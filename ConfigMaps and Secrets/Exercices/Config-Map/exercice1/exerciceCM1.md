# Utilisation d'un ConfigMap pour la configuration d'un reverse proxy

## 1. Context

Dans cette mise en pratique nous allons voir l'utilisation de l'objet ConfigMap pour fournir un fichier de configuration à un reverse proxy très simple que nous allons baser sur *nginx*.

Nous allons configurer ce proxy de façon à ce que les requètes reçues sur le endpoint */whoami* soient forwardées sur un service nommé *whoami*, tournant également dans le cluster. Ce service expose le endpoint */* et renvoie simplement le nom du container qui a traité la requète.

## 2. Création de l'application whoami

Définissez un ensemble de spécifications YAML pour créer un Pod nommé poddy et un Service nommé whoami. Le Pod doit contenir un seul container basé sur l'image lucj/whoami, tandis que le Service doit être de type ClusterIP et exposer le port 80 pour rediriger le trafic vers le Pod. Assurez-vous d'inclure les métadonnées appropriées pour les deux objets.
Copiez cette spécification dans un fichier *whoami.yaml* puis créez le Pod et le Service avec la commande suivante:
Appliquer les manifestes et vérifiez que les objets ont été correctement lancés.

## 3. Création d'une ConfigMap

Nous allons utiliser la configuration ci-dessous pour le serveur nginx que nous mettrons en place dans la suite.

```
user nginx;
worker_processes 4;
pid /run/nginx.pid;
events {
   worker_connections 768;
}
http {
  server {
    listen *:80;
    location = /whoami {
      proxy_pass http://whoami/;
    }
  }
}
```
Copiez cette configuration dans un fichier *nginx.conf* et créez ensuite la configMap *proxy-config* à partir du fichier **nginx.conf**:

## 4. Spécification du reverse-proxy

Créez un Pod nommé proxy avec les caractéristiques suivantes :

- Nom du Pod : proxy
- Image du container : nginx:1.20-alpine
- Un volume monté nommé config qui utilise une ConfigMap nommée proxy-config et est monté dans le chemin /etc/nginx/ du container *proxy*.
- De plus, créez un Service nommé proxy qui sélectionne le Pod proxy et expose le port 80 en utilisant un type NodePort avec le port NodePort défini sur 31600.


Copiez cette spécification dans un fichier *proxy.yaml* puis créez le Pod et le Service.

Vérifiez ensuite que ces 2 objets ont été correctement lancés/


## 5. Test de l'application

Depuis l'IP d'une des machines du cluster nous pouvons alors envoyer une requête GET sur le endpoint */whoami* et voir que celle-ci est bien traitée par l'application *whoami*, elle renvoie *poddy*, le nom du Pod.

Utilisez la commande `kubectl get nodes -o wide` pour obtenir les adresses IP des machines du cluster et remplacez *HOST_IP* par l'une d'entre elles:

```
$ curl HOST_IP:31600/whoami
```

## 6. Cleanup

Supprimez les différents ressources créées.
