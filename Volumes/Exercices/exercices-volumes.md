# Les Volumes 

Kubernetes prend en charge de nombreux types de volumes. Un Pod peut utiliser simultanément plusieurs types de volumes. Les volumes éphémères ont une durée de vie limitée à celle d'un pod, tandis que les volumes persistants existent au-delà de la durée de vie d'un pod. Lorsqu'un pod cesse d'exister, Kubernetes détruit les volumes éphémères ; cependant, Kubernetes ne détruit pas les volumes persistants. Pour tout type de volume dans un pod donné, les données sont préservées lors des redémarrages des conteneurs.

- [Volumes de configuration]
- [Volumes secrets]
- [Volumes éphémères]
- [Volumes persistants]

## Guide 1 : Volumes de configuration

Créer un nouveau fichier (nginx-3.yaml) définissant un pod **nginx-3** avec l'image **nginx** et monter le configmap **db-config-1** en tant que volume nommé **db-config** avec le chemin de montage **/config**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-3
spec:
  containers:
  - image: nginx
    name: nginx-3
    volumeMounts:
      - name: db-config
        mountPath: "/config"
        readOnly: true
  volumes:
    - name: db-config
      configMap:
        name: db-config-1
```
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: db-config-1
data:
  DB_HOST: "db.example.com"
  DB_PORT: "5432"
```
### Appliquer:

```yaml
kubectl apply -f nginx-3.yaml
```

### vérifier les variables d'environnement

```
kubectl exec nginx-3 -- cat /config/DB_HOST # vérifier les variables d'environnement
```
```
# db.example.com
```

## Guide 2: Volumes secrets

### Créer un nouveau fichier (nginx-4.yaml) définissant un pod `nginx-4` avec l'image `nginx` et monter le secret `db-secret-1` en tant que volume nommé `db-secret` avec le chemin de montage `/secret`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-4
spec:
  containers:
  - image: nginx
    name: nginx-4
    volumeMounts:
      - name: db-secret
        mountPath: "/secret"
        readOnly: true
  volumes:
    - name: db-secret
      secret:
        secretName: db-secret-1
```
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-secret-1
type: Opaque
data:
  DB_HOST: ZGIuZXhhbXBsZS5jb20=
  DB_PORT: NTQzMg==
```
### Appliquer:
```
kubectl apply -f nginx-4.yaml
```
### Vérifier les variables:
```bash
kubectl exec nginx-4 -- cat /secret/DB_HOST  # vérifier les variables d'environnement
# db.example.com
```

## Guide 3: Volumes éphémères

### Créer le pod redis (redis.yaml) avec l'image **redis** et le volume **redis-storage** en tant que stockage éphémère monté à **/data/redis**.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: redis
spec:
  containers:
  - name: redis
    image: redis
    volumeMounts:
    - name: redis-storage
      mountPath: /data/redis
  volumes:
  - name: redis-storage
    emptyDir: {} # Stockage éphémère
```
### Appliquer:
```
kubectl apply -f redis.yaml
```

## Exercice 4: Volumes persistants

### Créer un volume persistant nommé `app-data` dans un fichier (app-data.yaml), d'une capacité de `200Mi` et avec le mode d'accès `ReadWriteMany`. Le type de volume est `hostPath` et son emplacement est `/srv/app-data`.

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: app-data
spec:
  storageClassName: manual
  capacity:
    storage: 200Mi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/srv/app-data"
```
### Appliquer:
```yaml
kubectl apply -f app-data.yaml
```
### Vérifier:
```
kubectl get pv
```

```
# NAME       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
# app-data   200Mi      RWX            Retain           Available           manual
```


# Exercice 1:

### Créer un pod comme suit :
- Nom : non-persistent-redis
- Image du conteneur : redis
- Volume avec nom : cache-control
- Chemin de montage : /data/redis
- Le pod doit être lancé dans l'espace de noms staging et le volume ne doit pas être persistant.

# Exercice 2:
### Créer les éléments suivants :
- PV `task-pv-volume` avec un stockage de `10Mi`, un mode d'accès `ReadWriteOnce` sur `hostpath` `/mnt/data`.
- PVC `task-pv-claim` pour utiliser le PV.
- Créer un pod `task-pv-pod` avec l'image `nginx` pour utiliser le PVC monté sur `/usr/share/nginx/html`

### Obtenir les classes de stockage (les classes de stockage n'appartiennent pas à un espace de noms)

### Nettoyer les ressources créées.
