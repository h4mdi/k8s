# Correction Exercice 1:

### Créer un pod comme suit :
- Nom : non-persistent-redis
- Image du conteneur : redis
- Volume avec nom : cache-control
- Chemin de montage : /data/redis
- Le pod doit être lancé dans l'espace de noms staging et le volume ne doit pas être persistant.

### Définir un ns:
```yaml
kubectl create namespace staging
```

### Définition du fichier : **non-persistent-redis.yaml**

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: non-persistent-redis
  namespace: staging
spec:
  containers:
  - name: redis
    image: redis
    volumeMounts:
    - name: cache-control
      mountPath: /data/redis
  volumes:
  - name: cache-control
    emptyDir: {}
```
### Appliquer:
```
kubectl apply -f non-persistent-redis.yaml
```
# Correction Exercice 2:

### Créer les éléments suivants :
- PV `task-pv-volume` avec un stockage de `10Mi`, un mode d'accès `ReadWriteOnce` sur `hostpath` `/mnt/data`.
- PVC `task-pv-claim` pour utiliser le PV.
- Créer un pod `task-pv-pod` avec l'image `nginx` pour utiliser le PVC monté sur `/usr/share/nginx/html`

### Définition du fichier (task-pv-volume.yaml):

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume
spec:
  storageClassName: manual
  capacity:
    storage: 10Mi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```
### Application:

```
kubectl apply -f task-pv-volume.yaml
```

```
kubectl get pv
```
```
# NAME             CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
# task-pv-volume   10Mi       RWO            Retain           Available           manual                  6s
```
### Définition du fichier (task-pv-claim.yaml):

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Mi
```
### Application:

```
kubectl apply -f task-pv-claim.yaml
```
```
kubectl get pvc
```
```
#NAME            STATUS   VOLUME           CAPACITY   ACCESS MODES   STORAGECLASS   AGE
#task-pv-claim   Bound    task-pv-volume   10Mi       RWO            manual         12s
kubectl get pv # vérifier que le statut est 'Bound'
#NAME             CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                   STORAGECLASS   REASON   AGE
#task-pv-volume   10Mi       RWO            Retain           Bound    default/task-pv-claim   manual                  64s
```
### Définition du fichier (task-pv-pod.yaml):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: task-pv-pod
spec:
  volumes:
    - name: task-pv-storage
      persistentVolumeClaim:
        claimName: task-pv-claim
  containers:
    - name: task-pv-pod
      image: nginx
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/usr/share/nginx/html"
          name: task-pv-storage
```
### Application:

```
kubectl apply -f task-pv-pod.yaml
```

### Obtenir les classes de stockage (les classes de stockage n'appartiennent pas à un espace de noms)

```bash
kubectl get storageclass
# OU
kubectl get sc
```
### Nettoyage

```bash
kubectl delete pod task-pv-pod redis nginx-3 nginx-4 --force
kubectl delete pvc task-pv-claim
kubectl delete pv task-pv-volume
```
