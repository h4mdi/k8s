## Correction

**1. Création du Persistent Volume (PV) :**

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nginx-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /data/nginx
```

**Note**: Dans le bloc YAML fourni :
hostPath indique le type de stockage utilisé, qui est dans ce cas spécifique le stockage sur le nœud hôte.
path spécifie le chemin d'accès sur le nœud hôte où les données seront stockées.
```yaml
hostPath:
  path: /data/nginx
```

Appliquez le fichier YAML pour créer le PV dans le cluster Kubernetes :

```bash
kubectl apply -f nginx-pv.yaml
```

**2. Création du Persistent Volume Claim (PVC) :**

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nginx-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Appliquez le fichier YAML pour créer le PVC dans le cluster Kubernetes :
```bash
kubectl apply -f nginx-pvc.yaml
```

**3. Déploiement de NGINX avec Persistance des Données :**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        volumeMounts:
        - name: nginx-storage
          mountPath: /usr/share/nginx/html
      volumes:
      - name: nginx-storage
        persistentVolumeClaim:
          claimName: nginx-pvc
```

Appliquez le fichier YAML pour déployer NGINX dans le cluster Kubernetes :
```bash
kubectl apply -f nginx-deployment.yaml
```

**4. Vérification :**
- Vérifiez que le déploiement de NGINX est opérationnel :
```bash
kubectl get pods
```
- Créez un fichier dans le répertoire de stockage persistant de NGINX pour simuler des données :
```bash
kubectl exec -it <nginx-pod-name> -- bash
echo "Hello, World!" > /usr/share/nginx/html/index.html
exit
```
- Supprimez le pod NGINX et vérifiez que les données persistentes sont toujours disponibles lorsque le pod est recréé :

```bash
kubectl delete pod <nginx-pod-name>
```
Vérifiez que le pod NGINX est recréé et que les données persistentes sont toujours présentes :

```bash
kubectl get pods
kubectl exec -it <nginx-pod-name> -- cat /usr/share/nginx/html/index.html
```
