Dans cette partie, nous allons déployer NGINX avec persistance des données en utilisant les Persistent Volumes (PV) et Persistent Volume Claims (PVC) de Kubernetes.

---

### 1- Comprendre les Volumes dans Kubernetes

Les Volumes en Kubernetes sont utilisés pour stocker des données que les Pods peuvent utiliser. Contrairement aux volumes Docker, les volumes Kubernetes peuvent exister au-delà de la durée de vie d'un conteneur individuel et peuvent être partagés entre plusieurs conteneurs au sein d'un même Pod.


#### a. Créez un fichier YAML pour un Pod simple avec un volume vide (`emptyDir`).

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-test
spec:
  containers:
  - name: busybox
    image: busybox
    command: [ "sh", "-c", "sleep 3600" ]
    volumeMounts:
    - name: my-volume
      mountPath: /data
  volumes:
  - name: my-volume
    emptyDir: {}
```

2. Appliquez le Pod à votre cluster Kubernetes :

```bash
kubectl apply -f volume-test.yaml
```

3. Vérifiez que le Pod est en cours d'exécution :

```bash
kubectl get pods
```

4. Entrez dans le Pod et vérifiez le volume :

```bash
kubectl exec -it volume-test -- sh
```

À l'intérieur du Pod, vous pouvez vérifier le contenu du répertoire `/data` :

```bash
ls /data
```

---

### 2- Mutualiser des données entre plusieurs Pods

Pour mutualiser des données entre plusieurs Pods, vous pouvez utiliser des volumes persistants qui permettent de stocker des données **au-delà** de la durée de vie d'un Pod.
- Un PersistentVolume (PV) est une ressource de stockage dans Kubernetes qui est provisionnée par un administrateur ou dynamiquement via des StorageClasses. Les PVs sont indépendants des namespaces et peuvent être utilisés par n'importe quel Pod.

- Un PersistentVolumeClaim (PVC) est une demande de stockage par un utilisateur. Un PVC spécifie la taille et les modes d'accès requis. Kubernetes lie automatiquement le PVC à un PV disponible qui correspond aux critères spécifiés.

a. Créez un PersistentVolume (PV) et un PersistentVolumeClaim (PVC).

#### Créer un Persistent Volume (PV)

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nginx-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

#### Créer un Persistent Volume Claim (PVC)

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nginx-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

b. Appliquez le PV et le PVC à votre cluster Kubernetes :

```bash
kubectl apply -f nginx-pv.yaml
kubectl apply -f nginx-pvc.yaml
```

3. Vérifiez que le PV et le PVC sont créés et liés :

```bash
kubectl get pv
kubectl get pvc
```

---

### 3- Déployer NGINX avec persistance des données

Pour déployer NGINX avec persistance des données, vous allez monter le PVC dans le Pod NGINX, ce qui permettra de stocker les données de manière persistante.

1. Créez un fichier YAML pour le Pod NGINX avec un PVC monté:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-persistent-pod
spec:
  containers:
  - name: nginx
    image: nginx:latest
    ports:
    - containerPort: 80
    volumeMounts:
    - name: nginx-storage
      mountPath: /usr/share/nginx/html
  volumes:
  - name: nginx-storage
    persistentVolumeClaim:
      claimName: nginx-pvc
```

2. Appliquez le Pod à votre cluster Kubernetes :

```bash
kubectl apply -f nginx-persistent-pod-default.yaml
```

3. Vérifiez que le Pod est en cours d'exécution :

```bash
kubectl get pods
```

4. Accédez au Pod NGINX pour vérifier que le volume est monté :

```bash
kubectl exec -it nginx-persistent-pod -- sh
```

À l'intérieur du Pod, vérifiez le contenu du répertoire `/usr/share/nginx/html` :

```bash
ls /usr/share/nginx/html
```
